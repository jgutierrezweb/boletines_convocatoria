<?php

namespace App\Listeners;

use Exception;
use App\Events\ConfirmNewsLetterEvent;
use App\Mail\ConfirmNewsLetterMail;
use App\Models\Boletin;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class ConfirmNewsLetterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ConfirmNewsLetterEvent $event)
    {
        try {            
            $boletin = Boletin::with(['plantilla', 'noticias', 'emails'])->findOrFail($event->newsletter);

            if(!empty($boletin))
            {
                foreach ($boletin->emails as $email) 
                {
                    Mail::to($email)->send(new ConfirmNewsLetterMail($boletin));

                    $email->pivot->estado = 0;
                    $email->pivot->save();
                }

                return true;
            }

        } catch (Exception $e) {            
            echo $e->getMessage();
        }    
    }
}

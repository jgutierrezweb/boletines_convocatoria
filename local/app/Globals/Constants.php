<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Globals;

/**
 * Description of Constantes
 *
 */
final class Constants 
{	
    /**
     * General
     */
    const STATUS_ACTIVE = 1;
    const NAME_STATUS_ACTIVE = 'Activo';
    const NAME_STATUS_INACTIVE = 'Inactivo';
    const LIMIT_REGISTROS_TABLE = 10;
    const LIMIT_LATEST_REGISTROS_TABLE = 5;
    const LIMIT_LIST_PRODUCTS = 15;

    const PORTAL_NEWS_BASE_URL = 'https://agencia.udistrital.edu.co';
    const PORTAL_NEWS_URL = 'https://agencia.udistrital.edu.co/noticias-principales.xml';

    /**
     * Version
     */
    const VERSION_SYSTEM = '1.0.0';

    /**
     * Roles
     */
    const NAME_ROLE_SUPERADMINISTRADOR = 'superadministrador';
    const NAME_ROLE_ADMINISTRADOR = 'administrador';
    const NAME_ROLE_PUBLISHER = 'publicador';

    /**
     * Estado Boletin
     */
    const NAME_STATUS_BULLETIN_PENDING = 'pendiente';
    const NAME_STATUS_BULLETIN_APPROVED = 'aprobado';
    const NAME_STATUS_BULLETIN_SENT = 'enviado';
    const NAME_STATUS_BULLETIN_CANCEL = 'anulado';
}
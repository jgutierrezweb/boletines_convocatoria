<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Globals;

final class Structures
{	
	/**
	 * Dias de la semana
	 * @var  array
	 */
	public static $dias = [
		'0' => 'Domingo',
		'1' => 'Lunes',
		'2' => 'Martes',
		'3' => 'Miercoles',
		'4' => 'Jueves',
		'5' => 'Viernes',
		'6' => 'Sabado'
	];

	/**
	 * Meses del ano
	 * @var array
	 */
	public static $meses = [
		'01' => 'Enero',
		'02' => 'Febrero',
		'03' => 'Marzo',
		'04' => 'Abril',
		'05' => 'Mayo',
		'06' => 'Junio',
		'07' => 'Julio',
		'08' => 'Agosto',
		'09' => 'Septiembre',
		'10' => 'Octubre',
		'11' => 'Noviembre',
		'12' => 'Diciembre',
	];

	public static $mesesc = [
		'1' => 'Enero',
		'2' => 'Febrero',
		'3' => 'Marzo',
		'4' => 'Abril',
		'5' => 'Mayo',
		'6' => 'Junio',
		'7' => 'Julio',
		'8' => 'Agosto',
		'9' => 'Septiembre',
		'10' => 'Octubre',
		'11' => 'Noviembre',
		'12' => 'Diciembre',
	];
	
    /**
     * Lista de sexo
     * @var array
     */
    public static $sexo = [
        'M' => 'Hombre',
        'F' => 'Mujer'
    ];
}
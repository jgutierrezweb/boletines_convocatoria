<?php

namespace App\Http\Controllers;

use Exception;
use App\Globals\Alerts;
use App\Http\Controllers\Logs\CreateLogsController;
use App\Models\Plantilla;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PlantillasController extends Controller
{
    public function __construct()
    {
        //code
    }

    public function index()
    {
        $plantillas = Plantilla::where('borrado_logico', '=', 0)->orderBy('id', 'ASC')->get();

        return view('plantillas.index', [
            'plantillas' => $plantillas,
        ]);
    }

    public function nuevo(Request $request)
    {
        if ($request->isMethod('post')) {
            try {

                DB::beginTransaction();

                $plantilla = new Plantilla;
                $plantilla->nombre = $request->post('nombre');
                $plantilla->descripcion = $request->post('descripcion');
                $plantilla->estado = $request->post('estado');
                $plantilla->save();

                //CreateLogsController::newUserLog($plantilla, 'INSERT');

                DB::commit();

                return redirect()->route('templates.index')
                    ->with('message', Alerts::CREATE_SUCCESS)
                    ->with('messageType', 'success');
                
            } catch (Exception $e) {
                DB::rollback();
                return false;
            }
        }
    }

    public function editarAjax(Request $request)
    {
        if ($request->ajax()) {
            $plantilla = Plantilla::findOrFail($request->get('plantillaId'));

            return response()->json($plantilla);
        } else {
            return abort(404);
        }
    }

    public function editar(Request $request)
    {
        if ($request->method('post')) {

            try {

                DB::beginTransaction();

                $plantilla = Plantilla::findOrFail($request->get('plantilla_id'));

                $plantilla->descripcion = $request->post('edit-descripcion');
                $plantilla->estado = $request->post('edit-estado');

                if ($request->hasFile('edit-image') && $request->file('edit-image')->isValid()) {
                    $file = $request->file('edit-image');
                    $filename = $plantilla->slug . '.' . $file->getClientOriginalExtension();

                    Storage::disk('public')->putFileAs('plantillas', $file, $filename);

                    $plantilla->path_imagen = $filename;
                }

                $plantilla->save();

                DB::commit();

                return redirect()->route('templates.index')
                    ->with('message', Alerts::UPDATE_SUCCESS)
                    ->with('messageType', 'success');

            } catch (Exception $e) {
                DB::rollback();
                return false;
            }
        } else {
            return abort(404);
        }
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Socialite;
use Auth;
use Exception;
use App\User;

class GoogleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback()
    {
        try {

            $user = Socialite::driver('google')->user();
            $finduser = User::where('email', $user->getEmail())->where('borrado_logico', 0)->first();

            if($finduser) {
                if (!$finduser->status) {
                    return redirect('login')->with('error','<script type="text/javascript">setTimeout(function(){alert("No se pudo validar la identidad de este usuario o el usuario se encuentra desactivado.");},500)</script>');
                }
                
                $finduser->avatar = $user->getAvatar();
                $finduser->google_id = $user->id;
                $finduser->name = $user->name;
                $finduser->save();

                Auth::login($finduser);

                return redirect()->route('home.index');

            }else{
                return redirect('login')->with('error', '<script type="text/javascript">setTimeout(function(){alert("Usuario no se encuentra registrado en el sistema.");},500)</script>');
            }
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}
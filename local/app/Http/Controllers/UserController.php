<?php

namespace App\Http\Controllers;

use Exception;
use App\Globals\Alerts;
use App\Globals\Constants;
use App\User;
use App\Models\Role;
use App\Http\Controllers\Logs\CreateLogsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function __construct()
    {
        //code
    }

    public function index() 
    {
        $usuarios = User::whereHas('roles', function ($q) {
            $q->where('name', '<>',Constants::NAME_ROLE_SUPERADMINISTRADOR);
        })->where('borrado_logico', '=', 0)->orderBy('id', 'DESC')->with(['roles'])->get();
        
        $roles = Role::where('name', '<>', Constants::NAME_ROLE_SUPERADMINISTRADOR)->orderBy('id', 'ASC')->get();

        return view('users.index', [
            'roles'    => $roles,
            'usuarios' => $usuarios
        ]);
    }

    public function nuevo(Request $request) 
    {
        if ($request->isMethod('post'))
        {
            try {

                DB::beginTransaction();
                
                $usuario = new User;
                $usuario->name = $request->post('name');
                $usuario->email = $request->post('email');
                $usuario->password = bcrypt('dummy' . rand(10000,40000));
                $usuario->status = $request->post('estado');
                $usuario->save();

                $role = Role::where('name', $request->post('tipo_usuario'))->firstOrFail();
                $usuario->attachRole($role);

                //CreateLogsController::newUserLog($usuario, $role->display_name, 'INSERT');

                DB::commit();

                return redirect()->route('users.index')
                    ->with('message', Alerts::CREATE_SUCCESS)
                    ->with('messageType', 'success');

            } catch (Exception $e) {
                DB::rollback();
                return false;
            }            
        }
    }

    public function editarAjax(Request $request)
    {
        if ($request->ajax()) {
            $user = User::with(['roles'])->findOrFail($request->get('usuarioId'));

            return response()->json($user);
        } else {
            return abort(404);
        }
    }

    public function editar(Request $request) 
    {
        if ($request->method('post')) 
        {
            try {

                DB::beginTransaction();
                
                $usuario = User::with(['roles'])->findOrFail($request->get('usuario_id'));
                $rolUsuario = $usuario->roles->first();
                
                $usuario->name = $request->post('edit-name');
                $usuario->email = $request->post('edit-email');
                $usuario->status = $request->post('status');
                $usuario->password = bcrypt('dummy' . rand(10000,40000));
                $usuario->save();

                //Si se le cambio el rol al usuario
                if ($rolUsuario->name !== $request->post('tipo_usuario')) {
                    $usuario->detachRole($rolUsuario);

                    $role = Role::where('name', $request->post('tipo_usuario'))->firstOrFail();
                    $usuario->attachRole($role);

                    //CreateLogsController::newUserLog($usuario, $role->display_name, 'UPDATE');
                } else {
                    //CreateLogsController::newUserLog($usuario, $rolUsuario->display_name, 'UPDATE');
                }

                DB::commit();

                return redirect()->route('users.index')
                    ->with('message', Alerts::UPDATE_SUCCESS)
                    ->with('messageType', 'success');
                
            } catch (Exception $e) {
                DB::rollback();
                return false;
            } 

        } else {
            return abort(404);
        }
    }
}

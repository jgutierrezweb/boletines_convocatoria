<?php

namespace App\Http\Controllers;

use App\Events\ConfirmNewsLetterEvent;
use Exception;
use App\Globals\Alerts;
use App\Globals\Constants;
use App\Imports\NewslettersEmailImport;
use App\Models\Boletin;
use App\Models\BoletinEstado;
use App\Models\BoletinNoticia;
use App\Models\Email;
use App\Models\Plantilla;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class BoletinesController extends Controller
{
    public function __construct()
    {
        //code
    }

    public function index()
    {
        $boletines = Boletin::where('borrado_logico', '=', 0)->when(auth()->user()->hasRole(Constants::NAME_ROLE_PUBLISHER), function ($q) {
            return $q->where('user_id', auth()->user()->id);
        })->orderBy('id', 'DESC')->with(['estado', 'plantilla', 'user'])->get();

        return view('boletines.index', [
            'boletines' => $boletines
        ]);
    }

    public function create()
    {
        $plantillas = Plantilla::where('borrado_logico', '=', 0)->orderBy('id', 'ASC')->get();

        return view('boletines.create', [
            'plantillas' => $plantillas
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->except(['_token']);
        $emailsList = [];

        if ($request->hasFile('file')) {

            try {

                DB::beginTransaction();

                $validator = Validator::make($request->all(), [
                    'nombre'      => 'required|string',
                    'descripcion' => 'nullable|string',
                    'asunto'      => 'required|string',
                    'plantilla'   => 'required|string',
                    'file'        => 'required|mimes:xlsx,xls'
                ]);

                if ($validator->fails()) {
                    return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
                }
            
                $rows = Excel::toArray(new NewslettersEmailImport, request()->file('file'));

                if(count($rows) > 0) {
                    foreach ($rows[0] as $email) {
                        if($email[0]) {
                            $emailsList[] = $email[0];
                        }
                    }
                }

                if (count($emailsList) === 0) {
                    $validator->getMessageBag()->add('Lista de Contactos', 'El archivo debe poseer al menos un mail para poder continuar');

                    return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
                }                

                $plantilla = Plantilla::where('slug', $data['plantilla'])->firstOrFail();
                $boletinEstadoPendiente = BoletinEstado::where('slug', Constants::NAME_STATUS_BULLETIN_PENDING)->firstOrFail();

                $boletin = Boletin::create([
                    'nombre'            => $data['nombre'],
                    'asunto'            => $data['asunto'],
                    'descripcion'       => $data['descripcion'],
                    'plantilla_id'      => $plantilla->id,
                    'boletin_estado_id' => $boletinEstadoPendiente->id,
                    'user_id'           => auth()->user()->id
                ]);

                foreach ($data['index'] as $index) {
                    BoletinNoticia::create([
                        'nombre'      => $data['titulos'][$index],
                        'descripcion' => $data['resumenes'][$index],
                        'enlace'      => $data['enlaces'][$index],
                        'path_imagen' => $data['imagenes'][$index],
                        'boletin_id'  => $boletin->id
                    ]);
                }

                foreach ($emailsList as $mail) {
                    $email = Email::firstOrCreate([
                        'email' => $mail
                    ], [
                        'borrado_logico' => 0
                    ]);

                    $boletin->emails()->attach($email);
                }

                DB::commit();

                return redirect()->route('newsletters.index')
                    ->with('message', Alerts::CREATE_SUCCESS)
                    ->with('messageType', 'success');

            } catch (Exception $e) {
                echo $e->getMessage();
                DB::rollback();
                return false;
            }
        }
    }

    public function edit($id)
    {
        $boletin = Boletin::with(['plantilla'])->findOrFail($id);

        if (auth()->user()->hasRole(Constants::NAME_ROLE_PUBLISHER)) {
            $this->authorize('passFilterNewsletter', $boletin);
        }

        $plantillas = Plantilla::where('borrado_logico', '=', 0)->orderBy('id', 'ASC')->get();

        return view('boletines.edit', [
            'boletin'    => $boletin,
            'plantillas' => $plantillas
        ]);
    }

    public function update($id, Request $request)
    {
        try {
            
            DB::beginTransaction();

            $data = $request->except(['_token', 'plantilla']);
            $boletin = Boletin::findOrFail($id);
            
            if (auth()->user()->hasRole(Constants::NAME_ROLE_PUBLISHER)) {
                $this->authorize('passFilterNewsletter', $boletin);
            }

            $validator = Validator::make($request->all(), [
                'nombre'      => 'required|string',
                'descripcion' => 'nullable|string',
                'asunto'      => 'required|string',
                'plantilla'   => 'required|string'
            ]);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
                    
            $plantilla = Plantilla::where('slug', $request->post('plantilla'))->firstOrFail();
            $data['plantilla_id'] = $plantilla->id;

            $boletin->update($data);

            DB::commit();
            
            return redirect()->route('newsletters.index')
                ->with('message', Alerts::UPDATE_SUCCESS)
                ->with('messageType', 'success');

        } catch (Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function preview($id)
    {
        $boletin = Boletin::findOrFail($id);

        if (auth()->user()->hasRole(Constants::NAME_ROLE_PUBLISHER)) {
            $this->authorize('passFilterNewsletter', $boletin);
        }

        if(!empty($boletin->contenido)) {
            return $boletin->contenido;
        } else {
            $boletin->load(['plantilla', 'noticias']);            

            return view('emails.' . $boletin->plantilla->slug, [
                'boletin' => $boletin
            ]);
        }
    }

    public function confirm($id)
    {
        try {

            DB::beginTransaction();

            $boletin = Boletin::with(['plantilla', 'noticias'])->findOrFail($id);            
            $boletinEstadoAprobado = BoletinEstado::where('slug', Constants::NAME_STATUS_BULLETIN_APPROVED)->firstOrFail();

            $htmlBoletin = view('emails.' . $boletin->plantilla->slug, ['boletin' => $boletin])->render();

            Boletin::findOrFail($id)->update([
                'fecha_enviado'     => now(),
                'boletin_estado_id' => $boletinEstadoAprobado->id,
                'contenido'         => $htmlBoletin,                 
            ]);

            //event or jobs
            event(new ConfirmNewsLetterEvent($id));

            DB::commit();

            return redirect()->route('newsletters.index')
                ->with('message', Alerts::NEWSLETTER_CONFIRM)
                ->with('messageType', 'success');

        } catch (Exception $e) {
            DB::rollback();
            return false;
        }    
    }

    public function cancel($id)
    {
        try {

            DB::beginTransaction();

            $boletin = Boletin::findOrFail($id);

            if (auth()->user()->hasRole(Constants::NAME_ROLE_PUBLISHER)) {
                $this->authorize('passFilterNewsletter', $boletin);
            }

            $boletinEstadoAnulado = BoletinEstado::where('slug', Constants::NAME_STATUS_BULLETIN_CANCEL)->firstOrFail();

            $boletin->update([
                'boletin_estado_id' => $boletinEstadoAnulado->id
            ]);

            DB::commit();

            return redirect()->route('newsletters.index')
                ->with('message', Alerts::NEWSLETTER_CANCEL)
                ->with('messageType', 'success');
                
        } catch (Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function news($boletinId)
    {
        $boletin = Boletin::with(['noticias'])->findOrFail($boletinId);

        if (auth()->user()->hasRole(Constants::NAME_ROLE_PUBLISHER)) {
            $this->authorize('passFilterNewsletter', $boletin);
        }

        return view('boletines.news', [
            'boletin' => $boletin
        ]);
    }

    public function updateNew($boletinId, $id, Request $request)
    {
        $boletin = Boletin::findOrFail($boletinId);

        if (auth()->user()->hasRole(Constants::NAME_ROLE_PUBLISHER)) {
            $this->authorize('passFilterNewsletter', $boletin);
        }

        try {

            DB::beginTransaction();

            $data = $request->except(['_token']);
            $result = BoletinNoticia::where('id', $id)->update($data);

            DB::commit();
            
            return response()->json([
                'status'  => 'OK',
                'message' => Alerts::UPDATE_SUCCESS
            ]);

        } catch (Exception $e) {
            DB::rollback();

            return response()->json([
                'status'  => 'ERROR',
                'message' => Alerts::ERROR_505
            ]);
        }
    }

    public function markFeaturedNew($boletinId, $id, Request $request)
    {
        $boletin = Boletin::findOrFail($boletinId);

        if (auth()->user()->hasRole(Constants::NAME_ROLE_PUBLISHER)) {
            $this->authorize('passFilterNewsletter', $boletin);
        }

        try {

            DB::beginTransaction();

            $data = $request->except(['_token']);
            //Clean Old New Featured
            BoletinNoticia::where('boletin_id', $boletinId)->update(['principal' => 0]);
            //Add New Featured
            BoletinNoticia::where('id', $id)->update(['principal' => 1]);

            DB::commit();

            return response()->json([
                'status'  => 'OK',
                'message' => 'La noticia ha sido marcada como principal'
            ]);
        } catch (Exception $e) {
            DB::rollback();

            return response()->json([
                'status'  => 'ERROR',
                'message' => Alerts::ERROR_505
            ]);
        }
    }

    public function deleteNew($boletinId, $id, Request $request)
    {
        $boletin = Boletin::findOrFail($boletinId);

        if (auth()->user()->hasRole(Constants::NAME_ROLE_PUBLISHER)) {
            $this->authorize('passFilterNewsletter', $boletin);
        }

        try {

            DB::beginTransaction();

            BoletinNoticia::where('id', $id)->delete();

            DB::commit();
            return redirect()->route('newsletters.news.index', $boletinId);
            
        } catch (Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function emails($boletinId)
    {
        $boletin = Boletin::with(['emails'])->findOrFail($boletinId);

        if (auth()->user()->hasRole(Constants::NAME_ROLE_PUBLISHER)) {
            $this->authorize('passFilterNewsletter', $boletin);
        }

        return view('boletines.emails', [
            'boletin' => $boletin
        ]);
    }

    public function storeEmail($boletinId, Request $request)
    {
        $boletin = Boletin::with(['emails'])->findOrFail($boletinId);

        if (auth()->user()->hasRole(Constants::NAME_ROLE_PUBLISHER)) {
            $this->authorize('passFilterNewsletter', $boletin);
        }

        try {
            
            DB::beginTransaction();

            $data = $request->except(['_token']);

            $boletin->load('emails');

            if($boletin->emails->where('email', $data['email'])->count() === 0) {
                $email = Email::firstOrCreate([
                    'email' => $data['email']
                ], [
                    'borrado_logico' => 0
                ]);

                $boletin->emails()->attach($email);
            }
        
            DB::commit();
            return redirect()->route('newsletters.emails.index', $boletinId);

        } catch (Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function deleteEmail($boletinId, $id, Request $request)
    {
        $boletin = Boletin::findOrFail($boletinId);

        if (auth()->user()->hasRole(Constants::NAME_ROLE_PUBLISHER)) {
            $this->authorize('passFilterNewsletter', $boletin);
        }

        try {

            DB::beginTransaction();
                        
            $email = Email::findOrFail($id);
            $boletin->emails()->detach($email);

            DB::commit();
            return redirect()->route('newsletters.emails.index', $boletinId);

        } catch (Exception $e) {
            DB::rollback();
            return false;
        }
    }
}

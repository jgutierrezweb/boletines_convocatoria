<?php

namespace App\Http\Controllers\Logs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Monolog\Handler\IFTTTHandler;

class CreateLogsController extends Controller
{
    public static function newDocumentLog($documento, $action)
    {
        $documento = $documento->toArray();
        $documento['accion'] = $action;
        $documento['usuario'] = Auth::user()->name;
        $documento['documento_id'] = $documento['id'];

        unset($documento['user_id']);
        unset($documento['id']);

        DB::connection( 'boletines_contratacion_logs')
            ->table('documentos_logs')
            ->insert($documento);

    }

    public static function newConvocatoriaLog($convocatoria, $action)
    {
        $convocatoria = $convocatoria->toArray();
        $convocatoria['accion'] = $action;
        $convocatoria['usuario'] = Auth::user()->name;
        $convocatoria['convocatoria_id'] = $convocatoria['id'];

        unset($convocatoria['user_id']);
        unset($convocatoria['id']);

        DB::connection('boletines_contratacion_logs')
            ->table('boletines_contratacion_logs')
            ->insert($convocatoria);
    }

    public static function newTipoDcoumentoLog($tipoDocumento, $action)
    {
        $tipoDocumento = $tipoDocumento->toArray();
        $tipoDocumento['accion'] = $action;
        $tipoDocumento['usuario'] = Auth::user()->name;
        $tipoDocumento['tipo_documento_id'] = $tipoDocumento['id'];

        //unset($tipoDocumento['user_id']);
        unset($tipoDocumento['id']);

        DB::connection('boletines_contratacion_logs')
            ->table('tipos_documentos_logs')
            ->insert($tipoDocumento);

    }

    public static function newTipoConvocatoriaLog($tipoConvocatoria, $action){

        $tipoConvocatoria = $tipoConvocatoria->toArray();
        $tipoConvocatoria['accion'] = $action;
        $tipoConvocatoria['usuario'] = Auth::user()->name;
        $tipoConvocatoria['tipo_convocatoria_id'] = $tipoConvocatoria['id'];

        //unset($tipoDocumento['user_id']);
        unset($tipoConvocatoria['id']);

        DB::connection('boletines_contratacion_logs')
            ->table('tipos_boletines_contratacion_logs')
            ->insert($tipoConvocatoria);
    }

    public static function newUserLog($user, $role, $action)
    {
        $user = $user->toArray();

        $user['role'] = $role;
        $user['accion'] = $action;
        $user['usuario'] = Auth::user()->name;
        $user['user_id'] = $user['id'];

        if ($action === "UPDATE") {
            unset($user['email_verified_at']);
            unset($user['google_id']);
            unset($user['avatar']);

            unset($user['roles']);
        }

        unset($user['id']);

        DB::connection('boletines_contratacion_logs')
            ->table('users_logs')
            ->insert($user);
    }
}
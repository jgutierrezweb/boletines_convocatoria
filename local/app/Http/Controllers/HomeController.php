<?php

namespace App\Http\Controllers;

use App\Globals\Constants;
use App\Models\Boletin;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $boletines = Boletin::where('borrado_logico', '=', 0)->when(auth()->user()->hasRole(Constants::NAME_ROLE_PUBLISHER), function ($q) {
            return $q->where('user_id', auth()->user()->id);
        })->orderBy('id', 'DESC')->with(['estado', 'plantilla', 'user'])->limit(10)->get();

        return view('home.index', [
            'boletines' => $boletines
        ]);
    }
}

<?php

namespace App\Policies;

use App\Models\Boletin;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsletterPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function passFilterNewsletter(User $user, Boletin $newsletter)
    {
        return $user->id === $newsletter->user_id;
    }
}

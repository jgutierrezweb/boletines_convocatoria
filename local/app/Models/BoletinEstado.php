<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class BoletinEstado extends Model
{
    use Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'boletin_estado';

    protected $fillable = [
        'slug', 'nombre', 'estado'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nombre',
                'onUpdate' => true
            ]
        ];
    }

    public function boletinesEstado()
    {
        return $this->hasMany(\App\Models\Boletin::class, 'boletin_estado_id');
    }

    public function getStatus()
    {
        return ($this->estado) ? 'Activo' : 'Suspendido';
    }
}

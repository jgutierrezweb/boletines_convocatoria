<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Plantilla extends Model
{
    use Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'plantilla';

    protected $fillable = [
        'slug', 'nombre', 'descripcion', 'path_imagen', 'estado'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nombre',
                'onUpdate' => true
            ]
        ];
    }

    public function boletinesPlantilla()
    {
        return $this->hasMany(\App\Models\Boletin::class, 'plantilla_id');
    }

    public function getStatus()
    {
        return ($this->estado) ? 'Activo' : 'Suspendido';
    }
}

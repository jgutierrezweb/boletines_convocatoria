<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Boletin extends Model
{
    use Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'boletin';

    protected $fillable = [
        'slug', 'nombre', 'asunto', 'descripcion', 'fecha_enviado', 'plantilla_id', 'boletin_estado_id', 'user_id', 'contenido', 'observaciones'
    ];

    protected $dates = [
        'fecha_enviado', 'created_at', 'updated_at'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nombre',
                'onUpdate' => true
            ]
        ];
    }

    public function plantilla()
    {
        return $this->belongsTo(\App\Models\Plantilla::class, 'plantilla_id');
    }

    public function estado()
    {
        return $this->belongsTo(\App\Models\BoletinEstado::class, 'boletin_estado_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function noticias()
    {
        return $this->hasMany(\App\Models\BoletinNoticia::class, 'boletin_id');
    }

    public function emails()
    {
        return $this->belongsToMany(\App\Models\Email::class, 'boletin_email', 'boletin_id', 'email_id')->withPivot('estado');
    }
}

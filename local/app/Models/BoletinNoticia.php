<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class BoletinNoticia extends Model
{
    use Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'boletin_noticia';

    protected $fillable = [
        'slug', 'nombre', 'descripcion', 'enlace', 'path_imagen', 'boletin_id', 'principal', 'estado', 'borrado_logico'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nombre',
                'onUpdate' => true
            ]
        ];
    }

    public function boletin()
    {
        return $this->belongsTo(\App\Models\Boletin::class, 'boletin_id');
    }
}

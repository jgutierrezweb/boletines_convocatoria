<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'email';

    protected $fillable = [
        'email'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function boletinesEmail()
    {
        return $this->hasMany(\App\Models\BoletinEmail::class, 'email_id');
    }

    public function emails()
    {
        return $this->belongsToMany(\App\Models\Boletin::class, 'boletin_email', 'email_id', 'boletin_id');
    }
}

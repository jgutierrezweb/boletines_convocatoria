<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BoletinEmail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'boletin_email';

    protected $fillable = [
        'boletin_id', 'email_id', 'estado'
    ];

    public function boletin()
    {
        return $this->belongsTo(\App\Models\Boletin::class, 'boletin_id');
    }

    public function email()
    {
        return $this->belongsTo(\App\Models\Email::class, 'email_id');
    }
}

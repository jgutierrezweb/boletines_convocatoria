@extends('layouts.page')

@section('breadcrumb_content')
    <ol class="breadcrumb breadcrumb-alt float-right">
        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Inicio</a></li>
    </ol>
@endsection

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    @role(['superadministrador', 'administrador'])
                        <h1>Últimos Boletines</h1>
                    @endrole

                    @role(['publicador'])
                        <h1>Mis Últimos Boletines</h1>
                    @endrole
                </div>
                <div class="col-sm-6">
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body table-responsive">
                            <table id="example1" class="table table-bordered dataTable" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">#ID</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Nombre</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Asunto</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Plantilla</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Responsable</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Estado</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Creado</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Modificado</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($boletines as $boletin)
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{ $boletin->id }}</td>
                                            <td>{{ $boletin->nombre }}</td>
                                            <td>{{ $boletin->asunto }}</td>
                                            <td>{{ $boletin->plantilla->nombre }}</td>
                                            <td>{{ $boletin->user->name }}</td>
                                            <td>{{ $boletin->estado->nombre }}</td>   
                                            <td>{{ $boletin->created_at->format('d-m-Y') }}</td>
                                            <td>{{ $boletin->updated_at->format('d-m-Y') }}</td>
                                            <td class="text-center">
                                                <a href="{{ route('newsletters.preview', $boletin->id) }}" class="btn btn-default btn-sm btn-flat" title="Previsualizar" target="_blank">&nbsp;<i class="fas fa-eye"></i></a>
                                                                                                
                                                @if($boletin->estado->slug === Constants::NAME_STATUS_BULLETIN_PENDING)
                                                    <a href="{{ route('newsletters.edit', $boletin->id) }}" class="btn btn-default btn-sm btn-flat" title="Editar">&nbsp;<i class="fas fa-edit"></i></a>
                                                    <a href="{{ route('newsletters.news.index', $boletin->id) }}" class="btn btn-default btn-sm btn-flat" title="Listado de Noticias">&nbsp;<i class="fas fa-th-list"></i></a>

                                                    @role([Constants::NAME_ROLE_SUPERADMINISTRADOR, Constants::NAME_ROLE_ADMINISTRADOR])
                                                        <a href="{{ route('newsletters.confirm', $boletin->id) }}" class="btn btn-default btn-sm btn-flat btn-modal-post-confirmation" title="Confirmar y Enviar">&nbsp;<i class="fas fa-check-circle"></i></a>
                                                    @endrole
                                                @endif

                                                <a href="{{ route('newsletters.emails.index', $boletin->id) }}" class="btn btn-default btn-sm btn-flat" title="Listado de Correos">&nbsp;<i class="fas fa-mail-bulk"></i></a>

                                                @role([Constants::NAME_ROLE_SUPERADMINISTRADOR, Constants::NAME_ROLE_ADMINISTRADOR])
                                                    @if($boletin->estado->slug !== Constants::NAME_STATUS_BULLETIN_CANCEL)
                                                        <a href="{{ route('newsletters.cancel', $boletin->id) }}" class="btn btn-default btn-sm btn-flat btn-modal-post-confirmation" title="Anular">&nbsp;<i class="fas fa-ban"></i></a>
                                                    @endif

                                                    <a href="{{ url('borrar/boletin/' . $boletin->id) }}" class="btn btn-default btn-sm btn-flat btn-modal-confirmation" title="Eliminar">&nbsp;<i class="fas fa-trash"></i></a>
                                                @endrole
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">#ID</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Nombre</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Asunto</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Plantilla</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Responsable</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Estado</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Creado</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Modificado</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Acciones</th>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')

    <script>
        $('document').ready(function() {
            var table = $('.dataTable').DataTable({
                "responsive": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": "{{asset('Spanish.json')}}",
                }
            });
        });
    </script>

@endsection
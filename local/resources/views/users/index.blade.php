@extends('layouts.page')

@section('breadcrumb_content')
    <ol class="breadcrumb breadcrumb-alt float-right">
        <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Inicio</a></li>
        <li class="breadcrumb-item active">Usuarios</li>
    </ol>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Usuarios</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <div class="crud-button-list float-sm-right">
                        @role([Constants::NAME_ROLE_SUPERADMINISTRADOR, Constants::NAME_ROLE_ADMINISTRADOR])
                            <button class="btn btn-primary btn-flat btn-open-convocatoria-modal"><i class="fas fa-plus"></i>&nbsp;Nuevo</button>
                        @endrole
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">                    
                        <div class="card-body table-responsive">
                            <table id="example1" class="table table-bordered dataTable" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">#ID</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Nombre</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Email</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Rol</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Estado</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Creado</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($usuarios as $usuario)
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{ $usuario->id }}</td>
                                            <td>{{ $usuario->name }}</td>
                                            <td><a href="mailto:{{ $usuario->email }}">{{ $usuario->email }}</a></td>
                                            <td>{{ $usuario->roles->first()->display_name }}</td>
                                            
                                            @if($usuario->status === 1)
                                                <td><label for="" class="badge badge-success">ACTIVO</label></td>
                                            @else
                                                <td><label for="" class="badge badge-danger">INACTIVO</label></td>
                                            @endif

                                            <td>{{ $usuario->created_at->diffForHumans() }}</td>
                                            
                                            @role([Constants::NAME_ROLE_SUPERADMINISTRADOR, Constants::NAME_ROLE_ADMINISTRADOR])
                                                <td class="text-center">
                                                    <button  class="btn btn-default btn-sm btn-flat" id="btn-edit" onclick="editarUsuario({{$usuario->id}})"><i class="fas fa-edit"></i></button>
                                                    <a  class="btn btn-default btn-sm btn-flat btn-modal-confirmation" id="btn-delete" href="{{ url('borrar/users/' . $usuario->id) }}"><i class="fas fa-trash"></i></a>
                                                </td>
                                            @endrole
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">#ID</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Nombre</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Email</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Rol</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Estado</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Creado</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Acciones</th>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--NUEVA USUARIO MODAL-->
    <div class="modal fade" tabindex="-1" role="dialog" id="convocatoria-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nuevo usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" name="nueva" method="post" action="{{ route('users.store') }}">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Nombre(*)</label>
                                <input type="text" class="form-control" name="name" id="name" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Correo(*)</label>
                                <input type="email" class="form-control" name="email" id="email" required>
                                <small id="emailHelpBlock" class="form-text text-muted">
                                    Solo están permitidos los dominios:<br><b>@correo.udistrital.edu.co</b> y <b>@udistrital.edu.co</b>.
                                </small>
                            </div>                            
                            <div class="form-group">
                                <label for="tipo_usuario">Tipo usuario(*)</label>
                                <select name="tipo_usuario" id="tipo_usuario" class="form-control" required>                                    
                                    @foreach($roles as $role)
                                        <option value="{{ $role->name }}">{{ strtoupper($role->display_name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="estado">Estatus(*)</label>
                                <select name="estado" id="estado" class="form-control" required>
                                    <option value="1">ACTIVO</option>
                                    <option value="0">INACTIVO</option>
                                </select>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-sm btn-flat"><i class="fas fa-save"></i>&nbsp;Guardar</button>
                        <button type="button" class="btn btn-danger btn-sm btn-flat" data-dismiss="modal">&nbsp;Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--/ NUEVA USUARIO-->

    <!--EDITAR USUARIO MODAL-->
    <div class="modal fade" tabindex="-1" role="dialog" id="editar-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" name="nueva" method="post" action="{{ route('users.update') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">                
                        <input type="hidden" name="usuario_id" id="usuario_id">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="edit-name">Nombre(*)</label>
                                <input type="text" class="form-control" name="edit-name" id="edit-name" required>
                            </div>
                            <div class="form-group">
                                <label for="edit-email">Correo(*)</label>
                                <input type="text" class="form-control" name="edit-email" id="edit-email" required>
                                <small id="editEmailHelpBlock" class="form-text text-muted">
                                    Solo están permitidos los dominios:<br><b>@correo.udistrital.edu.co</b> y <b>@udistrital.edu.co</b>.
                                </small>
                            </div>                        
                            <div class="form-group">
                                <label for="edit-tipo_usuario">Tipo usuario(*)</label>
                                <select name="tipo_usuario" id="edit-tipo_usuario" class="form-control" required>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->name }}">{{ strtoupper($role->display_name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="edit-user-status">Estatus(*)</label>
                                <select name="status" id="edit-user-status" class="form-control" required>
                                    <option value="1">ACTIVO</option>
                                    <option value="0">INACTIVO</option>
                                </select>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-sm btn-flat"><i class="fas fa-save"></i> Guardar</button>
                        <button type="button" class="btn btn-danger btn-sm btn-flat" data-dismiss="modal"> Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--/ EDITAR USUARIO-->

@endsection

@section('script')
    <script>
        var legalDomains = {
            "-correo.udistrital.edu.co": true,
            "-udistrital.edu.co": true
        };

        function editarUsuario(usuarioId) 
        {
            var $editarModal = $('#editar-modal');

            $.ajax({
                type:'post',
                data: { usuarioId: usuarioId, _token: '{{csrf_token()}}'},
                dataType:'json',
                url: '{{ route("users.edit") }}',
                complete: function(data){
                    var user = data.responseJSON;

                    $editarModal.find('#usuario_id').val(usuarioId);
                    $editarModal.find('#edit-name').val(user.name);
                    $editarModal.find('#edit-email').val(user.email);
                    $editarModal.find('#edit-user-status').val(user.status).trigger('change');
                    $editarModal.find('#edit-tipo_usuario').val(user.roles[0].name).trigger('change');

                    $editarModal.modal('show');
                }
            });
        }

        $('document').ready(function() {

            var table = $('.dataTable').DataTable({
                "responsive": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": "{{asset('Spanish.json')}}",
                }
            });

            $('.btn-open-convocatoria-modal').on('click', function(event) {
                event.preventDefault();

                var $convocatoriaModal = $('#convocatoria-modal'); 

                //Clean Form
                $convocatoriaModal.find('input').val('');
                $convocatoriaModal.find('select').prop("selectedIndex", 0);

                //Open Modal
                $convocatoriaModal.modal('show');
            });

            $('#convocatoria-modal form').on('submit', function(event) {
                                
                var $form = $(this),
                    matches = $form.find('input[name="email"]').val().match(/@(.*)$/),
                    flag = false;
                
                if (matches) {
                    // matches[1] is the part after the @ sign in the email address
                    if (("-" + matches[1]) in legalDomains) {
                        // found the domain in the permitted list
                        flag = true;
                    }
                }

                if(!flag) {

                    alert('Solo están permitidos los dominios @correo.udistrital.edu.co y @udistrital.edu.co')

                    event.preventDefault();
                } else {
                    $form.append('{{csrf_field()}}');
                }
            });

            $('#editar-modal form').on('submit', function(event) {
                                
                var $form = $(this),
                    matches = $form.find('input[name="edit-email"]').val().match(/@(.*)$/),
                    flag = false;
                
                if (matches) {
                    // matches[1] is the part after the @ sign in the email address
                    if (("-" + matches[1]) in legalDomains) {
                        // found the domain in the permitted list
                        flag = true;
                    }
                }

                if(!flag) {
                    alert('Solo están permitidos los dominios @correo.udistrital.edu.co y @udistrital.edu.co')
                    event.preventDefault();
                }
            });
        });
    </script>

@endsection

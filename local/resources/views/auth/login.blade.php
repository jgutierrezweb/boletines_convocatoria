<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Boletines | Acceso</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<style>
    body{
        background-image: url('img/background-login.jpg') !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
    }
    .login-box{
        background-color: transparent;
    }
    .card {
        background-color: transparent;
    }
    .login-card-body{
        min-height: 350px;
        border-radius: 0px;
        background-color: #ffffffe0;
        color: #212529;
        border: none;
    }
    #login-title h2, p{
        text-align:center;
        margin-top: 20px;
    }
    #login-title > h2{
        font-size: 1.4rem;
        font-weight: bold;
    }
    .box-form-login {
        border: 2px #55555536 solid;
        padding: 20px;
    }

    .box-form-login label {
        font-weight: bold !important;
        color: #2b2b2b;
    }

    .box-form-login input {
        border-radius: 0 !important;
    }

    .btn-danger {
        background-color: #8c1919;
        border-color: #8c1919;
        border-radius: 0 !important;
    }

    @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
        .login-box {
            width: auto !important;
        }
    }
</style>
<body class="hold-transition login-page">
<div class="login-box">
    @if(session()->exists('error'))
       {{ session()->get('error') }}
    @endif
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body align-items-center">
            <div class="row">
                <div class="align-middle">
                    <div id="logo-container">
                        <img src="{{ asset('img/logo_black.png') }}" alt="" class="img-fluid">
                    </div>
                    <div id="login-title">
                        <p>Ingrese a su cuenta</p>
                    </div>
                    <div class="social-auth-links text-center mb-3">                    
                        <a href="{{ route('auth-google') }}" class="btn btn-block btn-danger">
                            <i class="fab fa-google-plus mr-2"></i> Iniciar sesion con Google+
                        </a>
                    </div>
                    <div class="box-form-login d-none">
                        <form>
                            <div class="form-group">
                                <label for="email">Username</label>
                                <input type="text" name="email" id="email" class="form-control input-lg" tabindex="1">
                            </div>
                            <div class="form-group">
                                <label for="password">Contraseña</label>
                                <input type="password" name="password" id="password" class="form-control input-lg" tabindex="2">
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-danger">Iniciar Sesión</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>
<script>
    $(function () {
        $('form').on('submit', function(event) {
            event.preventDefault();
        });
    });
</script>
</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" style="width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">
	<head>
		<meta charset="UTF-8">
		<meta content="width=device-width, initial-scale=1" name="viewport">
		<meta name="x-apple-disable-message-reformatting">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="telephone=no" name="format-detection">
		<title>Nueva plantilla de correo electrónico 2020-11-12</title>
		<!--[if (mso 16)]>
		<style type="text/css">a{text-decoration:none}</style>
		<![endif]--> <!--[if gte mso 9]>
		<style>sup{font-size:100% !important}</style>
		<![endif]--> <!--[if gte mso 9]> 
		<xml>
			<o:OfficeDocumentSettings>
				<o:AllowPNG></o:AllowPNG>
				<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]--> <!--[if !mso]><!-- -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet">
		<!--<![endif]-->
		<style type="text/css">.rollover:hover .rollover-first{max-height:0px!important}.rollover:hover .rollover-second{max-height:none!important}#outlook a{padding:0}.ExternalClass{width:100%}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%}.es-button{mso-style-priority:100!important;text-decoration:none!important}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}.es-desk-hidden{display:none;float:left;overflow:hidden;width:0;max-height:0;line-height:0;mso-hide:all}td .es-button-border:hover a.es-button-1{}td .es-button-border-2:hover{border-color:#fedb03 #fedb03 #fedb03 #fedb03!important;background:#002240!important}@media only screen and (max-width:600px){p, ul li, ol li,a{font-size:14px!important;line-height:150%!important}h1{font-size:25px!important;text-align:center;line-height:120%!important}h2{font-size:24px!important;text-align:center;line-height:120%!important}h3{font-size:20px!important;text-align:center;line-height:120%!important}h1 a{font-size:25px!important}h2 a{font-size:24px!important}h3 a{font-size:20px!important}.es-menu td a{font-size:14px!important}.es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a{font-size:16px!important}.es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a{font-size:14px!important}.es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a{font-size:12px!important}*[class="gmail-fix"]{display:none!important}.es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3{text-align:center!important}.es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3{text-align:right!important}.es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3{text-align:left!important}.es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img{display:inline!important}.es-button-border{display:block!important}a.es-button{font-size:20px!important;display:block!important;border-left-width:0px!important;border-right-width:0px!important}.es-btn-fw{border-width:10px 0px!important;text-align:center!important}.es-adaptive table,.es-btn-fw,.es-btn-fw-brdr,.es-left,.es-right{width:100%!important}.es-content table, .es-header table, .es-footer table,.es-content,.es-footer,.es-header{width:100%!important;max-width:600px!important}.es-adapt-td{display:block!important;width:100%!important}.adapt-img{width:100%!important;height:auto!important}.es-m-p0{padding:0px!important}.es-m-p0r{padding-right:0px!important}.es-m-p0l{padding-left:0px!important}.es-m-p0t{padding-top:0px!important}.es-m-p0b{padding-bottom:0!important}.es-m-p20b{padding-bottom:20px!important}.es-mobile-hidden,.es-hidden{display:none!important}tr.es-desk-hidden,td.es-desk-hidden,table.es-desk-hidden{width:auto!important;overflow:visible!important;float:none!important;max-height:inherit!important;line-height:inherit!important}tr.es-desk-hidden{display:table-row!important}table.es-desk-hidden{display:table!important}td.es-desk-menu-hidden{display:table-cell!important}.es-menu td{width:1%!important}table.es-table-not-adapt, .esd-block-html table{width:auto!important}table.es-social{display:inline-block!important}table.es-social td{display:inline-block!important}}@media screen and (max-width:9999px){.cboxcheck:checked + input + * .thumb-carousel{height:auto!important;max-height:none!important;max-width:none!important;line-height:0}.thumb-carousel span{font-size:0;line-height:0}.cboxcheck:checked + input + * .thumb-carousel .car-content{display:none;max-height:0px;overflow:hidden}.cbox0:checked + * .content-1, .thumb-carousel .cbox1:checked + span .content-1, .thumb-carousel .cbox2:checked + span .content-2, .thumb-carousel .cbox3:checked + span .content-3, .thumb-carousel .cbox4:checked + span .content-4, .thumb-carousel .cbox5:checked + span .content-5{display:block!important;max-height:none!important;overflow:visible!important}.thumb-carousel .thumb{cursor:pointer;display:inline-block!important;width:17.5%;margin:1% 0.61%;border:0px solid rgb(187, 187, 187)}.moz-text-html .thumb{display:none!important}.thumb-carousel .thumb:hover{border:0px solid rgb(68, 68, 68)}.cbox0:checked + * .thumb-1, .thumb-carousel .cbox1:checked + span .thumb-1, .thumb-carousel .cbox2:checked + span .thumb-2, .thumb-carousel .cbox3:checked + span .thumb-3, .thumb-carousel .cbox4:checked + span .thumb-4, .thumb-carousel .cbox5:checked + span .thumb-5{border-color:rgb(51, 51, 51)}.thumb-carousel .thumb img{width:100%;height:auto}.thumb-carousel img{max-height:none!important}.cboxcheck:checked + input + * .fallback{display:none!important;display:none;max-height:0px;height:0px;overflow:hidden}}@media screen and (max-width:600px){.car-table.responsive, .car-table.responsive .thumb-carousel, .car-table.responsive .thumb-carousel .car-content img, .car-table.responsive .fallback .car-content img{width:100%!important;height:auto}}@media screen{}</style>
	</head>
	<body style="width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">
		<div class="es-wrapper-color" style="background-color:#F6F6F6">
			<!--[if gte mso 9]> 
			<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
				<v:fill type="tile" color="#f6f6f6"></v:fill>
			</v:background>
			<![endif]-->
			<table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top">
				<tr style="border-collapse:collapse">
					<td valign="top" style="padding:0;Margin:0">
						<table cellpadding="0" cellspacing="0" class="es-header" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top">
							<tr style="border-collapse:collapse">
								<td align="center" bgcolor="transparent" style="padding:0;Margin:0;background-color:transparent">
									<table class="es-header-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
										<tr style="border-collapse:collapse">
											<td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;padding-bottom:20px;">
												<!--[if mso]>
												<table style="width:560px" cellpadding="0" cellspacing="0">
													<tr>
														<td style="width:270px" valign="top">
															<![endif]-->
															<table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
																<tr style="border-collapse:collapse">
																	<td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:270px">
																		<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																			<tr style="border-collapse:collapse">
																				<td align="center" style="padding:0;Margin:0;font-size:0px">
																					<a href="https://agencia.udistrital.edu.co" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#3D5CA3">
																						<img class="adapt-img" src="https://www.udistrital.edu.co/themes/custom/versh/logo.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="250">
																					</a>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!--[if mso]>
														</td>
														<td style="width:20px"></td>
														<td style="width:270px" valign="top">
															<![endif]-->
															<table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
																<tr style="border-collapse:collapse">
																	<td align="left" style="padding:0;Margin:0;width:270px">
																		<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																			<tr style="border-collapse:collapse">
																				<td align="left" style="padding:0;Margin:0">
																					<p style="Margin:0;margin-top:30px;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#333333;text-align:center;">
																						Boletin diciembre de 2020
																						<br>
																					</p>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!--[if mso]>
														</td>
													</tr>
												</table>
												<![endif]-->
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>

						@php
                           $news = $boletin->noticias;
                           
                           if($news->where('principal', 1)->isNotEmpty()) {
                              $firstNew = $news->where('principal', 1)->first();
                              
                              $news = $news->reject(function ($new, $key) {
                                 return $new->principal === 1;
                              });                        
                           } else {
                              $firstNew = $news->shift();
                           }
                        @endphp

						<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
							<tr style="border-collapse:collapse">
								<td align="center" bgcolor="transparent" style="padding:0;Margin:0;background-color:transparent">
									<table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
										<tr style="border-collapse:collapse">
											<td align="left" style="padding:0;Margin:0;background-position:center top">
												<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
													<tr style="border-collapse:collapse">
														<td align="center" valign="top" style="padding:0;Margin:0;width:600px">
															<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																<tr style="border-collapse:collapse">
																	<td align="center" style="padding:0;Margin:0;font-size:0px">
																		<a href="{{ $firstNew->enlace }}" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#C44D39">
																			<img class="adapt-img" src="{{ $firstNew->path_imagen }}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic">
																		</a>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						@if($news->isNotEmpty())
							<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
								<tr style="border-collapse:collapse">
									<td align="center" bgcolor="transparent" style="padding:0;Margin:0;background-color:transparent">
										<table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
											<tr style="border-collapse:collapse">
												<td align="left" style="Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;padding-bottom:25px;background-position:center top">
													<!--[if mso]>
													<table style="width:560px" cellpadding="0" cellspacing="0">
														<tr>
															<td style="width:270px" valign="top">
												<![endif]-->
												
												@php
													$newsAlt = $news;
													$firstNewAlt = $newsAlt->shift();
												@endphp

																<table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
																	<tr style="border-collapse:collapse">
																		<td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:270px">
																			<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																				<tr style="border-collapse:collapse">
																					<td align="center" height="25" style="padding:0;Margin:0"></td>
																				</tr>
																				<tr style="border-collapse:collapse">
																					<td align="left" class="es-m-txt-l" style="padding:0;Margin:0">
																						<h2 style="Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:24px;font-style:normal;font-weight:bold;color:#8C1919;text-align:center;">
																							{{ $firstNewAlt->nombre }}
																						</h2>
																					</td>
																				</tr>
																				<tr style="border-collapse:collapse">
																					<td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;font-size:0">
																						<table border="0" width="55%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																							<tr style="border-collapse:collapse">
																								<td style="padding:0;Margin:0;border-bottom:3px solid #FDB400;background:none;height:1px;width:100%;margin:0px"></td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																				<tr style="border-collapse:collapse">
																					<td align="left" style="padding:0;Margin:0">
																						<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#333333;text-align:justify;">
																							{{ $firstNewAlt->descripcion }}
																						</p>
																					</td>
																				</tr>
																				<tr style="border-collapse:collapse">
																					<td align="left" style="Margin:0;padding-top:10px;padding-bottom:10px;padding-left:15px;padding-right:15px">
																					<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:15px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:23px;color:#8C1919">
																						<strong>
																							<a href="{{ $firstNewAlt->enlace }}" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:15px;text-decoration:none;color:#8C1919">
																								Leer mas ➟
																							</a>
																						</strong>
																					</p>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
																<!--[if mso]>
															</td>
															<td style="width:20px"></td>
															<td style="width:270px" valign="top">
																<![endif]-->
																<table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
																	<tr style="border-collapse:collapse">
																		<td align="left" style="padding:0;Margin:0;width:270px">
																			<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																				<tr style="border-collapse:collapse">
																					<td align="center" style="padding:0;Margin:0;font-size:0px">
																						<a href="{{ $firstNewAlt->enlace }}">
																							<img class="adapt-img" src="{{ $firstNewAlt->path_imagen }}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="270">
																						</a>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
																<!--[if mso]>
															</td>
														</tr>
													</table>
													<![endif]-->
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						@endif
						<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
							<tr style="border-collapse:collapse">
								<td align="center" style="padding:0;Margin:0">
                           <table bgcolor="#fafafa" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FAFAFA;width:600px">
                              @if($boletin->noticias->count() > 1)
                                 <tr style="border-collapse:collapse">
                                    <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;background-position:center top;background-color:#FAFAFA" bgcolor="#fafafa">
                                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                          <tr style="border-collapse:collapse">
                                             <td align="center" valign="top" style="padding:0;Margin:0;width:560px">
                                                <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                   <tr style="border-collapse:collapse">
                                                      <td align="center" style="padding:0;Margin:0">
                                                         <h2 style="Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:24px;font-style:normal;font-weight:bold;color:#8C1919">
                                                            Noticias Relevantes
                                                         </h2>
                                                      </td>
                                                   </tr>
                                                   <tr style="border-collapse:collapse">
                                                      <td align="center" style="Margin:0;padding-bottom:5px;padding-top:10px;padding-left:10px;padding-right:10px;font-size:0">
                                                         <table border="0" width="15%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                            <tr style="border-collapse:collapse">
                                                               <td style="padding:0;Margin:0;border-bottom:3px solid #FDB400;background:none;height:1px;width:100%;margin:0px"></td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                    </td>
								 </tr>
								 
								@php
                                	$contNew = 0;
                              	@endphp

                                 @foreach($news as $key => $noticia)

                                    @if($loop->first || ($contNew % 2) === 0)
                                       <tr style="border-collapse:collapse">
                                          <td align="left" style="Margin:0;padding-bottom:5px;padding-top:10px;padding-left:20px;padding-right:20px;background-color:#FAFAFA;background-position:center top" bgcolor="#fafafa">
                                             <!--[if mso]>
                                             <table style="width:560px" cellpadding="0" cellspacing="0">
                                             <tr>
                                             <td style="width:272px" valign="top">
                                             <![endif]-->
                                    @endif
                                 
                                             <table cellpadding="0" cellspacing="0" class="{{ ($contNew % 2 === 0) ? 'es-left' : 'es-right' }}" align="{{ ($contNew % 2 === 0) ? 'left' : 'right' }}" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;{{ ($contNew % 2 === 0) ? 'float:left;' : 'float:right;' }}">
                                                <tr style="border-collapse:collapse">
                                                   <td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:272px">
                                                      <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;border-width:1px;border-style:solid;border-color:#EFEFEF;background-color:#FFFFFF" bgcolor="#ffffff" role="presentation">
                                                         <tr style="border-collapse:collapse">
                                                            <td align="center" style="padding:0;Margin:0;font-size:0px">
                                                               <a target="_blank" href="{{ $noticia->enlace }}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:#2CB543">
                                                                  <img class="adapt-img img-1" src="{{ $noticia->path_imagen }}" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;background:url(https://stripo.email/static//assets/img/default-img-back.png) 50% center no-repeat #F9F9F9;box-shadow:#EEEEEE 0px 0px 0px 1px inset" width="270">
                                                               </a>
                                                            </td>
                                                         </tr>
                                                         <tr style="border-collapse:collapse">
                                                            <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-left:15px;padding-right:15px">
                                                               <h4 style="Margin:0;line-height:120%;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;color:#002240">
                                                                  {{ $noticia->nombre }}
                                                               </h4>
                                                            </td>
                                                         </tr>
                                                         <tr style="border-collapse:collapse">
                                                            <td align="left" class="product-description" style="Margin:0;padding-top:5px;padding-bottom:10px;padding-left:15px;padding-right:15px">
                                                               <p class="product-description" style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#666666;text-align:justify;">
                                                                  {{ $noticia->descripcion }}
                                                               </p>
                                                            </td>
                                                         </tr>
                                                         <tr style="border-collapse:collapse">
                                                            <td align="left" style="Margin:0;padding-top:10px;padding-bottom:10px;padding-left:15px;padding-right:15px">
                                                               <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:15px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:23px;color:#8C1919">
                                                                  <strong>
                                                                     <a href="{{ $noticia->enlace }}" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:15px;text-decoration:none;color:#8C1919">
                                                                        Leer mas ➟
                                                                     </a>
                                                                  </strong>
                                                               </p>
                                                            </td>
                                                         </tr>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </table>
                                    
                                    @if($loop->last || (($contNew+1) % 2) === 0)
                                             <!--[if mso]>
                                                </td>
                                             </tr>
                                             </table>
                                             <![endif]-->
                                          </td>
                                       </tr>
									@endif
									
									@php
	                                    $contNew++;
                                 	@endphp
                                                                                       
                                 @endforeach
                              @endif

										<tr style="border-collapse:collapse">
											<td align="left" style="padding:0;Margin:0;padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px">
												<!--[if mso]>
												<table style="width:560px" cellpadding="0" cellspacing="0">
													<tr>
														<td style="width:270px" valign="top">
															<![endif]-->
															<table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
																<tr style="border-collapse:collapse">
																	<td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:270px">
																		<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																			<tr style="border-collapse:collapse">
																				<td align="center" style="padding:20px;Margin:0;font-size:0">
																					<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																						<tr style="border-collapse:collapse">
																							<td style="padding:0;Margin:0;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr style="border-collapse:collapse">
																				<td align="left" style="padding:0;Margin:0">
																					<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#333333">Institución de Educación Superior sujeta a inspección y vigilancia por el Ministerio de Educación Nacional</p>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!--[if mso]>
														</td>
														<td style="width:20px"></td>
														<td style="width:270px" valign="top">
															<![endif]-->
															<table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
																<tr style="border-collapse:collapse">
																	<td align="left" style="padding:0;Margin:0;width:270px">
																		<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																			<tr style="border-collapse:collapse">
																				<td align="center" style="padding:20px;Margin:0;font-size:0">
																					<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																						<tr style="border-collapse:collapse">
																							<td style="padding:0;Margin:0;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr style="border-collapse:collapse">
																				<td align="center" style="padding:0;Margin:0;font-size:0">
																					<table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																						<tr style="border-collapse:collapse">
																							<td align="center" valign="top" style="padding:0;Margin:0;padding-right:10px">
                                                                                                <a href="https://www.facebook.com/UniversidadDistrital" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#3D5CA3">
                                                                                                   <img src="https://nfgsqe.stripocdn.email/content/assets/img/social-icons/circle-colored/facebook-circle-colored.png" alt="Fb" title="Facebook" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic">
                                                                                                </a>
                                                                                             </td>
                                                                                             <td align="center" valign="top" style="padding:0;Margin:0;padding-right:10px">
                                                                                                <a href="https://twitter.com/udistrital" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#3D5CA3">
                                                                                                   <img src="https://nfgsqe.stripocdn.email/content/assets/img/social-icons/circle-colored/twitter-circle-colored.png" alt="Tw" title="Twitter" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic">
                                                                                                </a>
                                                                                             </td>
                                                                                             <td align="center" valign="top" style="padding:0;Margin:0;padding-right:10px">
                                                                                                <a href="https://www.instagram.com/universidaddistrital/" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#3D5CA3">
                                                                                                   <img src="https://nfgsqe.stripocdn.email/content/assets/img/social-icons/circle-colored/instagram-circle-colored.png" alt="Ig" title="Instagram" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic">
                                                                                                </a>
                                                                                             </td>
                                                                                             <td align="center" valign="top" style="padding:0;Margin:0">
                                                                                                <a href="https://www.youtube.com/user/udistritaltv" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#3D5CA3">
                                                                                                   <img src="https://nfgsqe.stripocdn.email/content/assets/img/social-icons/circle-colored/youtube-circle-colored.png" alt="Yt" title="Youtube" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic">
                                                                                                </a>
                                                                                             </td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!--[if mso]>
														</td>
													</tr>
												</table>
												<![endif]-->
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
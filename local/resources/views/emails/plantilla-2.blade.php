<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" style="width:100%;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">
	<head>
		<meta charset="UTF-8">
		<meta content="width=device-width, initial-scale=1" name="viewport">
		<meta name="x-apple-disable-message-reformatting">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="telephone=no" name="format-detection">
		<title>{{ $boletin->nombre }}</title>
		<!--[if (mso 16)]>
		<style type="text/css">a{text-decoration:none}</style>
		<![endif]--> <!--[if gte mso 9]>
		<style>sup{font-size:100% !important}</style>
		<![endif]--> <!--[if gte mso 9]> 
		<xml>
			<o:OfficeDocumentSettings>
				<o:AllowPNG></o:AllowPNG>
				<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]--> <!--[if !mso]><!-- -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">
		<!--<![endif]-->
		<style type="text/css">#outlook a{padding:0}.ExternalClass{width:100%}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%}.es-button{mso-style-priority:100!important;text-decoration:none!important}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}.es-desk-hidden{display:none;float:left;overflow:hidden;width:0;max-height:0;line-height:0;mso-hide:all}@media only screen and (max-width:600px){p, ul li, ol li,a{font-size:16px!important;line-height:150%!important}h1{font-size:30px!important;text-align:center;line-height:120%!important}h2{font-size:26px!important;text-align:center;line-height:120%!important}h3{font-size:20px!important;text-align:center;line-height:120%!important}h1 a{font-size:30px!important}h2 a{font-size:26px!important}h3 a{font-size:20px!important}.es-menu td a{font-size:16px!important}.es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a{font-size:16px!important}.es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a{font-size:16px!important}.es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a{font-size:12px!important}*[class="gmail-fix"]{display:none!important}.es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3{text-align:center!important}.es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3{text-align:right!important}.es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3{text-align:left!important}.es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img{display:inline!important}.es-button-border{display:block!important}a.es-button{font-size:20px!important;display:block!important;border-width:15px 25px 15px 25px!important}.es-btn-fw{border-width:10px 0px!important;text-align:center!important}.es-adaptive table,.es-btn-fw,.es-btn-fw-brdr,.es-left,.es-right{width:100%!important}.es-content table, .es-header table, .es-footer table,.es-content,.es-footer,.es-header{width:100%!important;max-width:600px!important}.es-adapt-td{display:block!important;width:100%!important}.adapt-img{width:100%!important;height:auto!important}.es-m-p0{padding:0px!important}.es-m-p0r{padding-right:0px!important}.es-m-p0l{padding-left:0px!important}.es-m-p0t{padding-top:0px!important}.es-m-p0b{padding-bottom:0!important}.es-m-p20b{padding-bottom:20px!important}.es-mobile-hidden,.es-hidden{display:none!important}tr.es-desk-hidden,td.es-desk-hidden,table.es-desk-hidden{width:auto!important;overflow:visible!important;float:none!important;max-height:inherit!important;line-height:inherit!important}tr.es-desk-hidden{display:table-row!important}table.es-desk-hidden{display:table!important}td.es-desk-menu-hidden{display:table-cell!important}.es-menu td{width:1%!important}table.es-table-not-adapt, .esd-block-html table{width:auto!important}table.es-social{display:inline-block!important}table.es-social td{display:inline-block!important}}</style>
	</head>
	<body style="width:100%;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">
		<div class="es-wrapper-color" style="background-color:#F6F6F6">
			<!--[if gte mso 9]> 
			<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
				<v:fill type="tile" color="#f6f6f6"></v:fill>
			</v:background>
			<![endif]-->
			<table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top">
				<tr style="border-collapse:collapse">
					<td valign="top" style="padding:0;Margin:0">
						<table class="es-header" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top">
							<tr style="border-collapse:collapse">
								<td style="padding:0;Margin:0;background-size:cover" align="center">
									<table class="es-header-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;width:640px" cellspacing="0" cellpadding="0" align="center">
										<tr style="border-collapse:collapse">
											<td align="left" style="padding:0;Margin:0;padding-top:10px;padding-left:20px;padding-right:20px">
												<!--[if mso]>
												<table style="width:600px" cellpadding="0" cellspacing="0">
													<tr>
														<td style="width:295px" valign="top">
															<![endif]-->
															<table cellspacing="0" cellpadding="0" align="left" class="es-left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
																<tr style="border-collapse:collapse">
																	<td class="es-m-p20b" valign="top" align="center" style="padding:0;Margin:0;width:295px">
																		<table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																			<tr style="border-collapse:collapse">
																				<td align="center" style="padding:0;Margin:0;font-size:0px">
																					<a href="https://agencia.udistrital.edu.co" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:20px;text-decoration:none;color:#B7BDC9">
																						<img src="https://www.udistrital.edu.co/themes/custom/versh/logo.png" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" alt="Logo" width="255">
																					</a>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!--[if mso]>
														</td>
														<td style="width:10px"></td>
														<td style="width:295px" valign="top">
															<![endif]-->
															<table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
																<tr style="border-collapse:collapse">
																	<td align="left" style="padding:0;Margin:0;width:295px">
																		<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																			<tr style="border-collapse:collapse">
																				<td align="center" style="padding:0;Margin:0">
																					<p style="Margin:0;margin-top:30px;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:20px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:30px;color:#8C1919">
																						{{ $boletin->nombre }}
																					</p>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!--[if mso]>
														</td>
													</tr>
												</table>
												<![endif]-->
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table class="es-content" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
							<tr style="border-collapse:collapse">
								<td align="center" style="padding:0;Margin:0">
									<table class="es-content-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#F6F6F6;width:640px" cellspacing="0" cellpadding="0" bgcolor="#f6f6f6" align="center">
										<tr style="border-collapse:collapse">
											<td align="left" style="padding:0;Margin:0;padding-top:10px;padding-left:20px;padding-right:20px">
												<table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
													<tr style="border-collapse:collapse">
														<td valign="top" align="center" style="padding:0;Margin:0;width:600px">
															<table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																<tr style="border-collapse:collapse">
																	<td align="center" style="padding:20px;Margin:0;font-size:0">
																		<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																			<tr style="border-collapse:collapse">
																				<td style="padding:0;Margin:0;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px"></td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr style="border-collapse:collapse">
																	<td align="left" style="padding:0;Margin:0">
																		<h1 style="Margin:0;padding-top:15px;padding-bottom:15px;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:32px;line-height:36px;font-weight:normal;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;color:#000000;text-align:center;">
																			Noticias relevantes
																		</h1>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						@foreach($boletin->noticias as $key => $noticia)  
							<!-- CONTENT -->
							<table class="es-content" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
								<tr style="border-collapse:collapse">
									<td align="center" style="padding:0;Margin:0">
										<table class="es-content-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;width:640px" cellspacing="0" cellpadding="0" align="center">
											<tr style="border-collapse:collapse">
												<td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px">
													<table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
														<tr style="border-collapse:collapse">
															<td valign="top" align="center" style="padding:0;Margin:0;width:600px">
																<table style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;border-radius:3px;background-color:#FFFFFF" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff" role="presentation">
																	<tr style="border-collapse:collapse">
																		<td align="center" style="padding:0;Margin:0;font-size:0px">
																			<a href="{{ $noticia->enlace }}" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:16px;text-decoration:none;color:#75B6C9">
																				<img class="adapt-img" src="{{ $noticia->path_imagen }}" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;border-radius:3px 3px 0px 0px" width="600">
																			</a>
																		</td>
																	</tr>
																	<tr style="border-collapse:collapse">
																		<td align="center" style="Margin:0;padding-bottom:5px;padding-left:20px;padding-right:20px;padding-top:25px">
																			<h2 style="Margin:0;line-height:24px;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:20px;font-style:normal;font-weight:bold;color:#8C1919">
																				{{ $noticia->nombre }}
																			</h2>
																		</td>
																	</tr>
																	<tr style="border-collapse:collapse">
																		<td align="center" style="Margin:0;padding-top:10px;padding-bottom:15px;padding-left:20px;padding-right:20px">
																			<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#000000">
																				<span class="product-description">{{ $noticia->descripcion }}</span>
																			</p>
																		</td>
																	</tr>
																	<tr style="border-collapse:collapse">
																		<td align="center" style="Margin:0;padding-left:10px;padding-right:10px;padding-top:15px;padding-bottom:25px">
																			<span class="es-button-border" style="border-style:solid;border-color:#75B6C9;background:#8C1919;border-width:1px;display:inline-block;border-radius:28px;width:auto">
																				<a href="{{ $noticia->enlace }}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:16px;color:#FFFFFF;border-style:solid;border-color:#8C1919;border-width:15px 25px 15px 25px;display:inline-block;background:#8C1919;border-radius:28px;font-weight:normal;font-style:normal;line-height:19px;width:auto;text-align:center">
																					Leer mas →
																				</a>
																			</span>
																		</td>
																	</tr>
																	<tr style="border-collapse:collapse">
																		<td align="center" style="padding:20px;Margin:0;font-size:0">
																			<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																				<tr style="border-collapse:collapse">
																					<td style="padding:0;Margin:0;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px"></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>							
							<!-- /CONTENT -->
						@endforeach
						<table cellpadding="0" cellspacing="0" class="es-footer" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top">
							<tr style="border-collapse:collapse">
								<td align="center" style="padding:0;Margin:0">
									<table class="es-footer-body" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#F6F6F6;width:640px">
										<tr style="border-collapse:collapse">
											<td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px">
												<!--[if mso]>
												<table style="width:600px" cellpadding="0" cellspacing="0">
													<tr>
														<td style="width:290px" valign="top">
															<![endif]-->
															<table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
																<tr style="border-collapse:collapse">
																	<td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:290px">
																		<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																			<tr style="border-collapse:collapse">
																				<td align="left" style="padding:0;Margin:0">
																					<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#333333">
																						Institución de Educación Superior sujeta a inspección y vigilancia por el Ministerio de Educación Nacional.
																					</p>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!--[if mso]>
														</td>
														<td style="width:20px"></td>
														<td style="width:290px" valign="top">
															<![endif]-->
															<table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
																<tr style="border-collapse:collapse">
																	<td align="left" style="padding:0;Margin:0;width:290px">
																		<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																			<tr style="border-collapse:collapse">
																				<td align="center" style="padding:0;Margin:0;font-size:0">
																					<table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
																						<tr style="border-collapse:collapse">
																							<td align="center" valign="top" style="padding:0;Margin:0;padding-right:10px">
																								<a href="https://www.facebook.com/UniversidadDistrita" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:none;color:#999999">
																									<img title="Facebook" src="https://ofcjno.stripocdn.email/content/assets/img/social-icons/circle-colored/facebook-circle-colored.png" alt="Fb" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic">
																								</a>
																							</td>
																							<td align="center" valign="top" style="padding:0;Margin:0;padding-right:10px">
																								<a href="https://twitter.com/udistrital" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:none;color:#999999">
																									<img title="Twitter" src="https://ofcjno.stripocdn.email/content/assets/img/social-icons/circle-colored/twitter-circle-colored.png" alt="Tw" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic">
																								</a>
																							</td>
																							<td align="center" valign="top" style="padding:0;Margin:0;padding-right:10px">
																								<a href="https://www.instagram.com/universidaddistrital/" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:none;color:#999999">
																									<img title="Instagram" src="https://ofcjno.stripocdn.email/content/assets/img/social-icons/circle-colored/instagram-circle-colored.png" alt="Inst" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic">
																								</a>
																							</td>
																							<td align="center" valign="top" style="padding:0;Margin:0">
																								<a href="https://www.youtube.com/user/udistritaltv" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:none;color:#999999">
																									<img title="Youtube" src="https://ofcjno.stripocdn.email/content/assets/img/social-icons/circle-colored/youtube-circle-colored.png" alt="Yt" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic">
																								</a>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!--[if mso]>
														</td>
													</tr>
												</table>
												<![endif]-->
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
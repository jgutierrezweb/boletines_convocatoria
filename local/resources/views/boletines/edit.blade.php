@extends('layouts.page')

@section('styles')
    <style>
        .accordion {
            max-height: 450px;
            overflow-y: scroll;
        }
        .accordion .card {
            border: none;
            margin-bottom: 2px;
            border-radius: 0;
            box-shadow: 0 0 6px rgba(0,0,0,0.2);
        }
        .accordion .card .card-header {
            background: #8c1919;
            padding-top: 7px;
            padding-bottom: 7px;
            border-radius: 0;
        }
        .accordion .card-header h2 {
            color: #fff;
            font-size: 1rem;
            font-weight: 500;
            font-family: "Roboto", sans-serif;
        }
        .accordion img {
            width: 200px;
        }
        .accordion .card-header h2 span {
            float: left;
            margin-top: 10px;
        }
        .accordion .card-header .btn {
            font-weight: 500;
            text-align: left;
            font-size: 85%;
        }
        .accordion .card-header i {
            color: #fff;
            font-size: 1.3rem;
            margin: 0 6px 0 -10px;
            font-weight: bold;
            position: relative;
            top: 5px;
        }			
        .accordion .card-header button:hover {
            color: #23384e;
        }
        .accordion .card-body {
            color: #666;
        }
        .accordion .card-body .media-body .title {
            text-align: center;
        }
        .accordion .card-body .media-body .description {
            text-align: justify;
        }

        /*
         * CheckBox
         */
        .custom-control-label::before, 
        .custom-control-label::after {
            top: .8rem;
            width: 1.25rem;
            height: 1.25rem;
        }
        
        /*
         * Buttons
         */
        .btn-template {
            background-color: #8c1919 !important;
            border-color: #8c1919 !important;
        }

        .btn-template.active {
            background-color: #343a40 !important;
            border-color: #343a40 !important;
        }
    </style>

@endsection

@section('breadcrumb_content')
    <ol class="breadcrumb breadcrumb-alt float-right">
        <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Inicio</a></li>
        <li class="breadcrumb-item"><a href="{{ route('newsletters.index') }}">Boletines</a></li>
        <li class="breadcrumb-item active">Nuevo</li>
    </ol>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            {!! Form::open(['id' => 'form-edit-newsletter', 'url' => route('newsletters.update', $boletin->id), 'method' => 'POST']) !!}

                @if ($errors->any())
                    <div class="alert alert-danger mt-3">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="row pt-3">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">                            
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="nombre">Nombre(*)</label>
                                                    <input type="text" class="form-control" name="nombre" id="nombre" value="{{ (old('nombre')) ? old('nombre') : $boletin->nombre }}" placeholder="Boletín Informativo 12-2020" required="required" tabindex="1">
                                                </div>
                                            </div>                                            
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="nombre">Asunto Boletin (*)</label>
                                                    <input type="text" class="form-control" name="asunto" id="asunto" value="{{ (old('asunto')) ? old('asunto') : $boletin->asunto }}" placeholder="Inserte un asunto acá" required="required" tabindex="2">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="descripcion">Descripción</label>
                                                    <textarea type="text" class="form-control" name="descripcion" id="descripcion" rows="4" placeholder="Coloque una descripción breve" tabindex="3">{{ (old('descripcion')) ? old('descripcion') : $boletin->descripcion }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
                <div class="row pt-3">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="text-center">Selecciona una plantilla para tu boletin</h5>
                            </div>
                            <div class="card-body">
                                <div class="card-group">
                                    @foreach($plantillas as $plantilla)
                                        <div class="card">
                                            @if(!empty($plantilla->path_imagen))
                                                <img class="card-img-top img-fluid" src="{{ asset('local/public/plantillas/' . $plantilla->path_imagen) }}" alt="Plantilla Referencial" width="400">
                                            @else
                                                <img class="card-img-top img-fluid" src="{{ asset('img/image-not-found.png') }}" alt="Image Not Found" width="400">
                                            @endif

                                            <div class="card-body">
                                                <h2 class="card-title w-100 text-center"><b>{{ $plantilla->nombre }}</b></h2>
                                            </div>
                                            <div class="card-footer">
                                                @if(old('plantilla'))
                                                    <button type="button" class="btn btn-danger btn-block btn-template {{ (old('plantilla') === $plantilla->slug) ? 'active' : null }}" data-template="{{ $plantilla->slug }}">{{ (old('plantilla') === $plantilla->slug) ? 'Seleccionado' : 'Seleccione' }}</button>
                                                @else
                                                    <button type="button" class="btn btn-danger btn-block btn-template {{ ($boletin->plantilla->slug === $plantilla->slug) ? 'active' : null }}" data-template="{{ $plantilla->slug }}">{{ ($boletin->plantilla->slug === $plantilla->slug) ? 'Seleccionado' : 'Seleccione' }}</button>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="section-hiddens">
                                    <input type="hidden" name="plantilla">
                                </div>
                                <div class="w-100 mt-5">
                                    <button type="submit" class="btn btn-primary float-right">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>

            {!! Form::close() !!}
        </div>
    </section>

@endsection

@section('script')
    @parent
    <script>
        $(document).ready(function() {

            // Code for the Validator
			var $validator = $('#form-edit-newsletter').validate({
				rules: {
					nombre: {
						required: true,
					},
                    descripcion: {
						required: false,
					},
                    asunto: {
						required: true,
					}                    
				},
				validClass : "success",
				highlight: function(element) {
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				},
				success: function(element) {
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
				}
			});

            $('.btn-template').on('click', function(event) {
                var $buttonTemplate = $(this); 

                if(!$buttonTemplate.hasClass('active')) {
                    //Remove old active class
                    $('.btn-template.active').removeClass('active').text('Selecciona');

                    //Add active class
                    $buttonTemplate.addClass('active').text('Seleccionado');
                }
            });

            $('#form-edit-newsletter').on('submit', function(event) {
                var $form = $(this);

                if($('.btn-template.active').length > 0) {
                    $form.find('input[name="plantilla"]').val($('.btn-template.active').attr('data-template'));
                } else { 
                    event.preventDefault();
                    flag = false;

                    Swal.fire(
				        'Error!',
				        'Debe seleccionar al menos una plantilla para poder continuar',
				        'error'
			        );
                }
            });
        });
    </script>
@endsection

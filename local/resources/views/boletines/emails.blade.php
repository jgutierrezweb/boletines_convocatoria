@extends('layouts.page')

@section('breadcrumb_content')
    <ol class="breadcrumb breadcrumb-alt float-right">
        <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Inicio</a></li>
        <li class="breadcrumb-item"><a href="{{ route('newsletters.index') }}">Boletines</a></li>
        <li class="breadcrumb-item">{{ $boletin->slug }}</li>
        <li class="breadcrumb-item active">Correos</li>
    </ol>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Listado de Correos</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <div class="crud-button-list float-sm-right">
                        <a href="" class="btn btn-primary btn-flat btn-add-email-modal"><i class="fas fa-plus"></i>&nbsp;Nuevo</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body table-responsive">
                            <table id="example1" class="table table-bordered dataTable" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Email</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Estado</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($boletin->emails as $key => $email)
                                        <tr role="row" class="odd">
                                            <td><a href="mailto:{{ $email->email }}">{{ $email->email }}</a></td>
                                            <td>
                                                @if(is_null($email->pivot->estado))
                                                    No Enviado
                                                @elseif($email->pivot->estado === 0)
                                                    Enviado
                                                @elseif($email->pivot->estado === 0)
                                                    Leído
                                                @else
                                                    &nbsp;
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('newsletters.emails.delete', [$boletin->id, $email->id]) }}" class="btn btn-default btn-sm btn-flat btn-modal-post-confirmation" title="Eliminar">&nbsp;<i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Email</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Estado</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Acciones</th>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--NUEVO EMAIL MODAL-->
    <div class="modal fade" tabindex="-1" role="dialog" id="convocatoria-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar Correo al Boletin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" name="nueva" method="post" action="{{ route('newsletters.emails.store', $boletin->id) }}">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="email">Email(*)</label>
                                <input type="email" class="form-control" name="email" id="email" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-sm btn-flat"><i class="fas fa-save"></i>&nbsp;Guardar</button>
                        <button type="button" class="btn btn-danger btn-sm btn-flat" data-dismiss="modal">&nbsp;Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--/ NUEVO EMAIL MODAL-->

@endsection

@section('script')

    <script>
        $('document').ready(function() {
            var table = $('.dataTable').DataTable({
                "responsive": true,
                "order": [[ 0, "asc" ]],
                "language": {
                    "url": "{{asset('Spanish.json')}}",
                }
            });
        });

        $('.btn-add-email-modal').on('click', function(event) {
            event.preventDefault();

            $modal = $('#convocatoria-modal');

            $modal.find('input[type="email"]').val('');
            $modal.modal('show');
        });
    </script>

@endsection

@extends('layouts.page')

@section('styles')
    <style>
        .box-mark-new-featured {
            position: absolute;
            top: 5px;
            left: 5px;
        }

        .box-mark-new-featured .btn-mark-featured {
            color: #aaa !important;
        }

        .box-mark-new-featured .btn-mark-featured i {
            font-size: 1.5em !important;
        }

        .box-mark-new-featured .btn-mark-featured.featured, .box-mark-new-featured .btn-mark-featured:hover {
            color: orange !important;
        }
    </style>

@endsection

@section('breadcrumb_content')
    <ol class="breadcrumb breadcrumb-alt float-right">
        <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Inicio</a></li>
        <li class="breadcrumb-item"><a href="{{ route('newsletters.index') }}">Boletines</a></li>
        <li class="breadcrumb-item">{{ $boletin->slug }}</li>
        <li class="breadcrumb-item active">Noticias</li>
    </ol>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Listado de Noticias</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6 d-none">
                    <div class="crud-button-list float-sm-right">
                        <a href="" class="btn btn-primary btn-flat btn-add-email-modal"><i class="fas fa-plus"></i>&nbsp;Nuevo</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                @foreach ($boletin->noticias as $noticia)
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="card">
                            {!! Form::open(['class' => 'form-news', 'url' => route('newsletters.news.update', [$boletin->id, $noticia->id]), 'method' => 'POST']) !!}
                                
                                <div class="box-mark-new-featured">
                                    <a href="{{ route('newsletters.news.mark-featured',  [$boletin->id, $noticia->id]) }}" class="btn btn-mark-featured {{ ($noticia->principal) ? 'featured' : null }}" title="Marcar como Principal">
                                        <i class="fa fa-star"></i>
                                    </a>
                                </div>

                                <a href="{{ $noticia->enlace }}" title="Abrir Noticia" target="_blank">
                                    <img src="{{ $noticia->path_imagen }}" class="card-img-top" alt="{{ $noticia->nombre }}">
                                </a>

                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="nombre">Título(*)</label>
                                                <input type="text" class="form-control" name="nombre" value="{{ $noticia->nombre }}" required="required" tabindex="1">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="nombre">Resumen(*)</label>
                                                <textarea name="descripcion" class="form-control" required="required" rows="3" tabindex="2">{{ $noticia->descripcion }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="nombre">Enlace(*)</label>
                                                <input type="text" class="form-control" name="enlace" value="{{ $noticia->enlace }}" required="required" readonly="readonlly" tabindex="3">
                                            </div>
                                        </div>
                                    </div>                                
                                </div>
                                <div class="card-footer">
                                    <div class="w-100 text-right">
                                        @if($boletin->noticias->count() > 1)
                                            <a href="{{ route('newsletters.news.delete', [$boletin->id, $noticia->id]) }}" class="btn btn-danger btn-modal-post-confirmation">Eliminar</a>
                                        @endif
                                        
                                        <button type="submit" class="btn btn-primary">Guardar</button>
                                    </div>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

@endsection

@section('script')

    <script>
        $('document').ready(function() {
            var table = $('.dataTable').DataTable({
                "responsive": true,
                "order": [[ 0, "asc" ]],
                "language": {
                    "url": "{{asset('Spanish.json')}}",
                }
            });

            $('.box-mark-new-featured .btn-mark-featured').on('click', function(event) {
                event.preventDefault();

                var $link = $(this);

                if(! $link.hasClass('featured')) {
                    Swal.fire({
                        title: "¿Estás seguro que desea marcar la noticia seleccionada como principal?",
                        text: "¡No podrás revertir esto!",
                        type: 'info',
                        showCancelButton: true,
                        cancelButtonColor: '#dc3545',
                        confirmButtonText: 'Confirmar',
                        cancelButtonText: 'Cancelar'
                    }).then((result) => {
                        if (result.value) {
                            $.post($link.attr('href'), {
                                _token: "{{ csrf_token() }}"
                            },
                                function (data, textStatus, jqXHR) {
                                    if(data.status === 'OK') {
                                        //Remove Old Featured Class
                                        $('.btn-mark-featured.featured').removeClass('featured');
                                        //Add New Featured Class
                                        $link.addClass('featured');
                                        
                                        Swal.fire(
                                            'Acción Completada!',
                                            data.message,
                                            'success'
                                        );
                                    } else {
                                        Swal.fire(
                                            'Error!',
                                            data.message,
                                            'error'
                                        );
                                    }
                                },
                                "json"
                            );
                        }
                    });
                }
            })

            $('.form-news').on('submit', function(event) {
                event.preventDefault();

                var $form = $(this);

                $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: $form.serialize(),
                    dataType: "json",
                    success: function (response) {
                        if(response.status === 'OK') {
                            Swal.fire(
                                'Acción Completada!',
                                response.message,
                                'success'
                            );
                        } else {
                            Swal.fire(
                                'Error!',
                                response.message,
                                'error'
                            );
                        }
                    }
                });
            })
        });
    </script>

@endsection

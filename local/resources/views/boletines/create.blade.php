@extends('layouts.page')

@section('styles')
    <style>
        .accordion {
            max-height: 450px;
            overflow-y: scroll;
        }
        .accordion .card {
            border: none;
            margin-bottom: 2px;
            border-radius: 0;
            box-shadow: 0 0 6px rgba(0,0,0,0.2);
        }
        .accordion .card .card-header {
            background: #8c1919;
            padding-top: 7px;
            padding-bottom: 7px;
            border-radius: 0;
        }
        .accordion .card-header h2 {
            color: #fff;
            font-size: 1rem;
            font-weight: 500;
            font-family: "Roboto", sans-serif;
        }
        .accordion img {
            width: 200px;
        }
        .accordion .card-header h2 span {
            float: left;
            margin-top: 10px;
        }
        .accordion .card-header .btn {
            font-weight: 500;
            text-align: left;
            font-size: 85%;
        }
        .accordion .card-header i {
            color: #fff;
            font-size: 1.3rem;
            margin: 0 6px 0 -10px;
            font-weight: bold;
            position: relative;
            top: 5px;
        }			
        .accordion .card-header button:hover {
            color: #23384e;
        }
        .accordion .card-body {
            color: #666;
        }
        .accordion .card-body .media-body .title {
            text-align: center;
        }
        .accordion .card-body .media-body .description {
            text-align: justify;
        }

        /*
         * CheckBox
         */
        .custom-control-label::before, 
        .custom-control-label::after {
            top: .8rem;
            width: 1.25rem;
            height: 1.25rem;
        }
        
        /*
         * Buttons
         */
        .btn-template {
            background-color: #8c1919 !important;
            border-color: #8c1919 !important;
        }

        .btn-template.active {
            background-color: #343a40 !important;
            border-color: #343a40 !important;
        }
    </style>

@endsection

@section('breadcrumb_content')
    <ol class="breadcrumb breadcrumb-alt float-right">
        <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Inicio</a></li>
        <li class="breadcrumb-item"><a href="{{ route('newsletters.index') }}">Boletines</a></li>
        <li class="breadcrumb-item active">Nuevo</li>
    </ol>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            {!! Form::open(['id' => 'form-create-newsletter', 'url' => route('newsletters.store'), 'method' => 'POST', 'files' => true]) !!}

                @if ($errors->any())
                    <div class="alert alert-danger mt-3">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="row pt-3">
                    <div class="col-md-4">
                        <div class="card" style="overflow-x: hidden;">
                            <div class="card-body">                            
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="nombre">Nombre(*)</label>
                                            <input type="text" class="form-control" name="nombre" id="nombre" value="{{ (old('nombre')) ? old('nombre') : null }}" placeholder="Boletín Informativo 12-2020" required="required" tabindex="1">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="descripcion">Descripción</label>
                                            <textarea type="text" class="form-control" name="descripcion" id="descripcion" rows="3" placeholder="Coloque una descripción breve" tabindex="2">{{ (old('descripcion')) ? old('descripcion') : null }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="asunto">Asunto Boletin (*)</label>
                                            <input type="text" class="form-control" name="asunto" id="asunto" value="{{ (old('asunto')) ? old('asunto') : null }}" placeholder="Inserte un asunto acá" required="required" tabindex="3">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="file">Listado de Contactos(*)</label>
                                            <input type="file" class="form-control" name="file" id="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required="required" tabindex="4" style="border: none;">
                                            <small id="archivoHelp" class="form-text text-muted">Formatos aceptados(.xls, .xlsx)</small>
                                            <small id="archivoExplainHelp" class="form-text text-muted mt-3" style="font-size: 100%;"><b>Debe utilizar la primera fila del documento para especificar los correos a registrar</b></small>
                                        </div>
                                    </div>                                
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <h5>
                                    Noticias Disponibles
                                    <br>
                                    <small>Seleccione las que desee agregar a un boletin</small>
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="accordion" id="accordionExample">                                
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
                <div class="row pt-3">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="text-center">Selecciona una plantilla para tu boletin</h5>
                            </div>
                            <div class="card-body">
                                <div class="card-group">
                                    @foreach($plantillas as $plantilla)
                                        <div class="card">
                                            @if(!empty($plantilla->path_imagen))
                                                <img class="card-img-top img-fluid" src="{{ asset('local/public/plantillas/' . $plantilla->path_imagen) }}" alt="Plantilla Referencial" width="400">
                                            @else
                                                <img class="card-img-top img-fluid" src="{{ asset('img/image-not-found.png') }}" alt="Image Not Found" width="400">
                                            @endif
                                            <div class="card-body">
                                                <h2 class="card-title w-100 text-center"><b>{{ $plantilla->nombre }}</b></h2>
                                            </div>
                                            <div class="card-footer">
                                                <button type="button" class="btn btn-danger btn-block btn-template {{ (old('plantilla') === $plantilla->slug) ? 'active' : null }}" data-template="{{ $plantilla->slug }}">{{ (old('plantilla') === $plantilla->slug) ? 'Seleccionado' : 'Seleccione' }}</button>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="section-hiddens">
                                    <input type="hidden" name="plantilla">
                                </div>
                                <div class="w-100 mt-5">
                                    <button type="submit" class="btn btn-primary float-right">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>

            {!! Form::close() !!}
        </div>
    </section>

@endsection

@section('script')
    @parent
    <script>
        $(document).ready(function() {

            var dataNewsletters = [];

            var portalNewsBaseURL = '{{ Constants::PORTAL_NEWS_BASE_URL }}',
                portalNewsURL = '{{ Constants::PORTAL_NEWS_URL }}';

            setTimeout(() => {
                getNewsXML();
            }, 500);

            // Code for the Validator
			var $validator = $('#form-create-newsletter').validate({
				rules: {
					nombre: {
						required: true,
					},
                    descripcion: {
						required: false,
					},
                    asunto: {
						required: true,
					},
                    file: {
                        required: true,
                        extension: "xls|xlsx"
                    }
				},
				validClass : "success",
				highlight: function(element) {
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				},
				success: function(element) {
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
				}
			});

            // Add minus icon for collapse element which is open by default
            $(".collapse.show").each(function(){
                $(this).siblings(".card-header").find(".btn i").removeClass("fa-plus").addClass('fa-minus');
            });
            
            // Toggle plus minus icon on show hide of collapse element
            $(".collapse").on('show.bs.collapse', function(){
                $(this).parent().find(".card-header .btn i").removeClass("fa-plus").addClass('fa-minus');
            }).on('hide.bs.collapse', function(){
                $(this).parent().find(".card-header .btn i").removeClass("fa-minus").addClass('fa-plus');
            });

            $('.btn-template').on('click', function(event) {
                var $buttonTemplate = $(this); 

                if(!$buttonTemplate.hasClass('active')) {
                    //Remove old active class
                    $('.btn-template.active').removeClass('active').text('Selecciona');

                    //Add active class
                    $buttonTemplate.addClass('active').text('Seleccionado');
                }
            });

            $('#form-create-newsletter').on('submit', function(event) {
              
                var $form = $(this),
                    $divSectionHiddens = $('.section-hiddens'),
                    flag = true;

                if($('.btn-template.active').length > 0) {
                    $form.find('input[name="plantilla"]').val($('.btn-template.active').attr('data-template'));
                } else { 
                    flag = false;

                    Swal.fire(
				        'Error!',
				        'Debe seleccionar al menos una plantilla para poder continuar',
				        'error'
			        );
                }

                if($('input[type=checkbox]:checked').length > 0) {
                    $('input[type=checkbox]:checked').each(function (index, element) {
                        $divSectionHiddens.append('<input type="hidden" name="index[]" value="' + index + '">');
                        $divSectionHiddens.append('<input type="hidden" name="titulos[]" value="' + dataNewsletters[$(element).attr('data-index')].titulo + '">');
                        $divSectionHiddens.append('<input type="hidden" name="resumenes[]" value="' + dataNewsletters[$(element).attr('data-index')].resumen + '">');
                        $divSectionHiddens.append('<input type="hidden" name="imagenes[]" value="' + dataNewsletters[$(element).attr('data-index')].imagen + '">');
                        $divSectionHiddens.append('<input type="hidden" name="enlaces[]" value="' + dataNewsletters[$(element).attr('data-index')].enlace + '">');
                    });
                } else {
                    flag = false;

                    Swal.fire(
				        'Error!',
				        'Debe seleccionar al menos una noticia disponible para continuar',
				        'error'
			        );
                }

                if(!flag) {
                    event.preventDefault();
                }
            });

            function getNewsXML()
            {
                $.ajax({
                    type: "GET" ,
                    url: portalNewsURL,
                    dataType: "xml" ,
                    success: function(xml) {
                        var $divAccordionExample = $('#accordionExample');

                        $(xml).find('item').each(function(index, item) {
                            $divAccordionExample.append(makeHtmlAccordeonNew(index, item));

                            //Add Item in Objects
                            dataNewsletters[index] = {
                                titulo: $(item).find('titulo').text(),
                                resumen: $(item).find('resumen').text(),
                                imagen:  portalNewsBaseURL + $(item).find('imagen').text(),
                                enlace: $(item).find('enlace').text()
                            };
                        });
                    }
                });
            }

            function makeHtmlAccordeonNew(index, item)
            {
                var htmlAccordeonNew = '',
                    showClassAccodeon = (index == 0) ? 'show' : '';

                htmlAccordeonNew += '<div class="card">';
                htmlAccordeonNew += '    <div class="card-header" id="heading-' + index + '">';
                htmlAccordeonNew += '        <h2 class="clearfix mb-0">';
                htmlAccordeonNew += '            <div class="row">';
                htmlAccordeonNew += '                <div class="col-10">';
                htmlAccordeonNew += '                    <a class="btn btn-link" data-toggle="collapse" data-target="#collapse-' + index +'" aria-expanded="true" aria-controls="collapse-' + index + '">';
                htmlAccordeonNew += '                        <i class="fas fa-plus"></i>&nbsp;' + $(item).find('titulo').text();
                htmlAccordeonNew += '                    </a>';
                htmlAccordeonNew += '                </div>';
                htmlAccordeonNew += '                <div class="col-2">';
                htmlAccordeonNew += '                    <div class="custom-control custom-checkbox float-right">';
                htmlAccordeonNew += '                        <input type="checkbox" class="custom-control-input checkbox-newsletters" id="custom-check-' + index + '" data-index="' + index + '">';
                htmlAccordeonNew += '                        <label class="custom-control-label" for="custom-check-' + index + '">&nbsp;</label>';
                htmlAccordeonNew += '                    </div>';
                htmlAccordeonNew += '                </div>';
                htmlAccordeonNew += '            </div>';
                htmlAccordeonNew += '        </h2>';
                htmlAccordeonNew += '    </div>';
                htmlAccordeonNew += '    <div id="collapse-' + index + '" class="collapse ' + showClassAccodeon + '" aria-labelledby="heading-' + index + '" data-parent="#accordionExample">';
                htmlAccordeonNew += '        <div class="card-body">';
                htmlAccordeonNew += '            <div class="media">';
                htmlAccordeonNew += '                <img src="'+ portalNewsBaseURL + $(item).find('imagen').text() +'" class="mr-3" alt="Image News ' + index + '">';
                htmlAccordeonNew += '                <div class="media-body">';
                htmlAccordeonNew += '                    <h5 class="title mt-0">' + $(item).find('titulo').text() + '</h5>';
                htmlAccordeonNew += '                    <p class="description mt-1">' + $(item).find('resumen').text() + '</p>';
                htmlAccordeonNew += '                    <a href="' + $(item).find('enlace').text() + '" class="mt-3 float-right" target="_blank">Leer mas...</a>';
                htmlAccordeonNew += '                </div>';
                htmlAccordeonNew += '            </div>';
                htmlAccordeonNew += '        </div>';
                htmlAccordeonNew += '    </div>';
                htmlAccordeonNew += '</div>';

                return htmlAccordeonNew;
            }
        });
    </script>
@endsection

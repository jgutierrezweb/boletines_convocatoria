<?php
    use Illuminate\Support\Facades\DB;
    use \App\UserLog;
?>

@extends('layouts.page')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Panel de logs</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home.index') }}"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Panel de logs</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Usuarios</a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-convocatorias" role="tab" aria-controls="nav-profile" aria-selected="false">Convocatorias</a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-documentos" role="tab" aria-controls="nav-contact" aria-selected="false">Documentos</a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-tipo-convocatorias" role="tab" aria-controls="nav-contact" aria-selected="false">Tipo convocatorias</a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-tipo-documentos" role="tab" aria-controls="nav-contact" aria-selected="false">Tipo documentos</a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="table-responsive">
                                        <table class="table dataTable">
                                            <thead>
                                                <tr>
                                                    <?php
                                                        $users_logs = new UserLog();
                                                        $users_logs_cols = $users_logs->getTableColumns();
                                                    ?>
                                                    
                                                    @foreach($users_logs_cols as $col)
                                                        <th>{{$col}}</th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach(UserLog::all()->toArray() as $item)
                                                    <tr>
                                                        @foreach($users_logs_cols as $col)

                                                            <td>{{$item[$col]}}</td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>                                  
                                </div>
                                <div class="tab-pane fade" id="nav-convocatorias" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <div class="table-responsive">
                                        <table class="table dataTable">
                                            <thead>
                                            <tr>
                                                <?php
                                                    $convocatorias_logs = new ConvocatoriaLog();
                                                    $convocatorias_logs_cols = $convocatorias_logs->getTableColumns();
                                                ?>

                                                @foreach($convocatorias_logs_cols as $col)
                                                    <th>{{$col}}</th>
                                                @endforeach
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach(ConvocatoriaLog::all()->toArray() as $item)
                                                <tr>
                                                @foreach($convocatorias_logs_cols as $col)

                                                    <td>{{$item[$col]}}</td>
                                                @endforeach
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-documentos" role="tabpanel" aria-labelledby="nav-contact-tab">
                                    <div class="table-responsive">
                                        <table class="table dataTable">
                                            <thead>
                                            <tr>
                                                <?php
                                                $documentos_logs = new DocumentoLog();
                                                $documentos_logs_cols = $documentos_logs->getTableColumns();
                                               
                                                ?>
                                                @foreach($documentos_logs_cols as $col)
                                                    <th>{{$col}}</th>
                                                @endforeach
                                            </tr>

                                            </thead>
                                            <tbody>
                                            @foreach(DocumentoLog::all()->toArray() as $item)
                                                <tr>
                                                    @foreach($documentos_logs_cols as $col)

                                                        <td>{{$item[$col]}}</td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-tipo-convocatorias" role="tabpanel" aria-labelledby="nav-contact-tab">
                                    <div class="table-responsive">
                                        <table class="table dataTable">
                                            <thead>
                                            <tr>
                                                <?php
                                                $tipos_convocatorias_logs = new TipoConvocatoriaLog();
                                                $tipos_convocatorias_logs_cols = $tipos_convocatorias_logs->getTableColumns();
                                                ?>
                                                @foreach($tipos_convocatorias_logs_cols as $col)
                                                    <th>{{$col}}</th>
                                                @endforeach
                                            </tr>

                                            </thead>
                                            <tbody>
                                            @foreach(TipoConvocatoriaLog::all()->toArray() as $item)
                                                <tr>
                                                    @foreach($tipos_convocatorias_logs_cols as $col)

                                                        <td>{{$item[$col]}}</td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-tipo-documentos" role="tabpanel" aria-labelledby="nav-contact-tab">
                                    <div class="table-responsive">
                                        <table class="table dataTable">
                                            <thead>
                                            <tr>
                                                <?php
                                                $tipos_documentos_logs = new TipoDocumentoLog();
                                                $tipos_documentos_logs_cols = $tipos_documentos_logs->getTableColumns();
                                                ?>
                                                @foreach($tipos_documentos_logs_cols as $col)
                                                    <th>{{$col}}</th>
                                                @endforeach
                                            </tr>

                                            </thead>
                                            <tbody>
                                            @foreach(TipoDocumentoLog::all()->toArray() as $item)
                                                <tr>
                                                    @foreach($tipos_documentos_logs_cols as $col)

                                                        <td>{{$item[$col]}}</td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('scripts')
    <script>
        $('document').ready(function(){
            $('.table').DataTable({
                "paging": true,
                "pageLength": 50,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "order": [[ 5, "desc" ]],
                "info": true,
                "autoWidth": false,
                "language": {
                    "url": "{{asset('Spanish.json')}}",
                }

            });
        })
    </script>
@endsection
@include('partials.header')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    @yield('content')
</div>

@include('partials.footer')
<script>
    $('document').ready(function(){
        $('.select2').select2();
    })
</script>
@yield('script')
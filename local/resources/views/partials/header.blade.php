<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title,', 'Boletines')</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/sweetalert2/sweetalert2.min.css') }}">
    <!-- Multiselect -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/multiselect/multi-select.css') }}">

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <!--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">-->

    <style>
        .custom-header {
            min-height: 80px;
            background-color:#8c1919;
            border-bottom: 3px solid #FFB800;
        }
        
        .layout-navbar-fixed .wrapper .main-header {
            position: relative !important;
        }

        .layout-navbar-fixed .wrapper .brand-link {
            position: relative !important;
        }

        .layout-navbar-fixed .wrapper .content-wrapper {
            margin-top: 0 !important;
        }

        .custom-header #header-logo img {
            max-height: 100px !important;
        }

        .breadcrumb-alt {
            background-color: #fff;
            padding: .5rem 1rem;
            margin-top: .75rem;
        }

        .tabla-convocatorias td {
            padding: 0.35rem !important;
        }

        .text-truncate {
            max-width: 150px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }

        .table-alt th {
            text-align: center;
        }

        @media screen and (min-width: 1600px) {
            .text-truncate {
                max-width: 400px;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }   
        }
    </style>

    @yield('styles')
</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed">
    <header class="custom-header">
        <nav class="navbar navbar-expand">
            <div class="container-fluid p-1">
                <div class="col-6 col-sm-6 col-md-4">
                    <div id="header-logo">
                        <a href="{{ route('home.index') }}">
                            <img src="{{ asset('img/logo.png') }}" alt="" class="img-fluid img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-6 col-lg-7">
                    <div class="row float-right">
                        <div class="col-xs-12">
                            <a href="{{ url('auth/logout') }}" class="btn btn-danger nav-link mb-2"><i class="fas fa-sign-out-alt"></i>&nbsp;Cerrar Sesión</a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <!-- Site wrapper -->
    <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <div class="col-4 col-sm-6">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
        </div>
        <div class="col-8 col-sm-6">
            @yield('breadcrumb_content', '')
        </div>
    </nav>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{ route('home.index') }}" class="brand-link elevation-4">
            <img src="{{ auth()->user()->avatar }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light"> {{ auth()->user()->name }} </span>
        </a>
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item {{ request()->routeIs('newsletters.*') ? 'menu-open' : null }}">
                        <a href="{{ route('newsletters.index') }}" class="nav-link">
                            <i class="nav-icon fas fa-newspaper"></i>
                            <p>
                                Boletines
                            </p>
                        </a>
                    </li>                    

                    @role([Constants::NAME_ROLE_SUPERADMINISTRADOR, Constants::NAME_ROLE_ADMINISTRADOR])
                        <li class="nav-item {{ request()->routeIs('templates.*') ? 'menu-open' : null }}">
                            <a href="{{ route('templates.index') }}" class="nav-link">
                                <i class="nav-icon fas fa-file"></i>
                                <p>
                                    Plantillas
                                </p>
                            </a>
                        </li>
                        <li class="nav-item {{ request()->routeIs('users.*') ? 'menu-open' : null }}">
                            <a href="{{ route('users.index') }}" class="nav-link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>
                                    Usuarios
                                </p>
                            </a>
                        </li>
                    @endrole                    
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
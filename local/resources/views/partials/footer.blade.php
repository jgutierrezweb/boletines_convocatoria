<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<form id="post-request-form" method="POST">
    @csrf
</form>

<!-- jQuery -->
<script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('adminlte/dist/js/demo.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/plugins/select2/js/select2.js') }}"></script>
<!-- SweetAlert2 -->
<script src="{{ asset('adminlte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- Multiselect -->
<script src="{{ asset('adminlte/plugins/multiselect/multi-select.js') }}"></script>
<!-- Jquery Validate -->
<script src="{{ asset('adminlte/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/jquery-validation/additional-methods.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/jquery-validation/localization/messages_es.min.js') }}"></script>
<script type="text/javascript">
	$(function() {
        $(document).on('click', '.btn-modal-confirmation', function(event) {
			
			event.preventDefault();
					
			var $link = $(this);
				
			Swal.fire({
				title: '¿Estás seguro que deseas borrar el registro seleccionado?',
				text: "¡No podrás revertir esto!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#007bff',
				cancelButtonColor: '#dc3545',
				confirmButtonText: 'Confirmar',
				cancelButtonText: 'Cancelar'
			}).then((result) => {
				if (result.value) {
					$(location).attr('href', $link.attr('href'));
				}
			});            
		});

		$(document).on('click', '.btn-modal-post-confirmation', function(event) {
			event.preventDefault();
					
			var $link = $(this);
				
			Swal.fire({
				title: '¿Estás seguro que desea continuar con la acción seleccionada?',
				text: "¡No podrás revertir esto!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#007bff',
				cancelButtonColor: '#dc3545',
				confirmButtonText: 'Confirmar',
				cancelButtonText: 'Cancelar'
			}).then((result) => {
				if (result.value) {
					$('#post-request-form').attr('action', $link.attr('href')).submit();
				}
			});
		});
	});
</script>

@if(session()->has('messageType'))
	@if(session()->get('messageType') == 'success')
	    <script>
            Swal.fire(
				'Acción Completada!',
				'{{ session()->get("message") }}',
				'success'
			);
	    </script>
	@elseif(session()->get('messageType') == 'error')
	    <script>
			Swal.fire(
				'Acción no completada!',
				'{{ session()->get("message") }}',
				'error'
			);
	    </script>
	@endif
@endif

</body>
</html>

@extends('layouts.page')

@section('breadcrumb_content')
    <ol class="breadcrumb breadcrumb-alt float-right">
        <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Inicio</a></li>
        <li class="breadcrumb-item active">Plantillas</li>
    </ol>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Plantillas</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    &nbsp;
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body table-responsive">
                            <table id="example1" class="table table-bordered dataTable" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending">#ID</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending">Nombre</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Descripción</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Imagen Referencial</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Estado</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Creado</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($plantillas as $plantilla)
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{ $plantilla->id }}</td>
                                            <td>{{ $plantilla->nombre }}</td>
                                            <td>{{ $plantilla->descripcion }}</td>

                                            @if(!empty($plantilla->path_imagen))
                                                <td>
                                                    <a href="{{ asset('local/public/plantillas/' . $plantilla->path_imagen) }}" target="_blank">Ver Imagen</a>
                                                </td>
                                            @else
                                                <td>&nbsp;</td>
                                            @endif

                                            @if($plantilla->estado === 1)
                                                <td><label for="" class="badge badge-success">ACTIVO</label></td>
                                            @else
                                                <td><label for="" class="badge badge-danger">INACTIVO</label></td>
                                            @endif

                                            <td>{{ $plantilla->created_at->format('d-m-Y') }}</td>
                                            
                                            <td class="text-center">
                                                <button class="btn btn-default btn-sm btn-flat" id="btn-edit" onclick="editarPlantilla({{ $plantilla->id }})"><i class="fas fa-edit"></i></button>
                                                <a class="btn btn-default btn-sm btn-flat btn-modal-confirmation d-none" id="btn-delete" href="{{ url('borrar/plantilla/' . $plantilla->id) }}"><i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending">#ID</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending">Nombre</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Descripción</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Imagen Referencial</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Estado</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Creado</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Acciones</th>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--EDITAR PLANTILLA MODAL-->
    <div class="modal fade" tabindex="-1" role="dialog" id="editar-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Plantilla</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" name="nueva" method="post" action="{{ route('templates.update') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">                
                        <input type="hidden" name="plantilla_id" id="plantilla_id">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="edit-nombre">Nombre(*)</label>
                                <input type="text" class="form-control" name="edit-nombre" id="edit-nombre" readonly="readonly" required="required">
                            </div>
                            <div class="form-group">
                                <label for="edit-descripcion">Descripción(*)</label>
                                <textarea type="text" class="form-control" name="edit-descripcion" id="edit-descripcion"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="edit-image">Imagen Referencial(*)</label>
                                <input type="file" name="edit-image" id="edit-image" accept="image/*">
                            </div>
                            <div class="form-group">
                                <label for="edit-estado">Estatus(*)</label>
                                <select name="edit-estado" id="edit-estado" class="form-control" required="required">
                                    <option value="1">ACTIVO</option>
                                    <option value="0">INACTIVO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-sm btn-flat"><i class="fas fa-save"></i>&nbsp;Guardar</button>
                        <button type="button" class="btn btn-danger btn-sm btn-flat" data-dismiss="modal">&nbsp;Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--/ EDITAR USUARIO-->

@endsection

@section('script')
    <script>
        function editarPlantilla(plantillaId) 
        {
            var $editarModal = $('#editar-modal');

            $.ajax({
                type:'post',
                data: { plantillaId: plantillaId, _token: '{{csrf_token()}}'},
                dataType: 'json',
                url: '{{ route("templates.edit") }}',
                complete: function(data){
                    var plantilla = data.responseJSON;

                    $editarModal.find('#plantilla_id').val(plantillaId);
                    $editarModal.find('#edit-nombre').val(plantilla.nombre);
                    $editarModal.find('#edit-descripcion').text(plantilla.descripcion);
                    $editarModal.find('#edit-estado').val(plantilla.estado).trigger('change');

                    $editarModal.modal('show');
                }
            });
        }

        $('document').ready(function() {
            var table = $('.dataTable').DataTable({
                "responsive": true,
                "order": [[ 0, "asc" ]],
                "language": {
                    "url": "{{asset('Spanish.json')}}",
                }
            });

            $('.btn-open-convocatoria-modal').on('click', function(event) {
                event.preventDefault();

                var $convocatoriaModal = $('#convocatoria-modal'); 

                //Clean Form
                $convocatoriaModal.find('input').val('');
                $convocatoriaModal.find('select').prop("selectedIndex", 0);

                //Open Modal
                $convocatoriaModal.modal('show');
            });

            $('#convocatoria-modal form').on('submit', function(event) {                            
                var $form = $(this);
                    
                $form.append('{{csrf_field()}}');
            });
        });
    </script>

@endsection

<?php

use App\Globals\Constants;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name'         => Constants::NAME_ROLE_SUPERADMINISTRADOR,
                'display_name' => 'Super Administrador',
                'description' => 'Has Special full access to the entire system',
                'created_at'   => date('Y-m-d H:i:s'),
                'updated_at'   => date('Y-m-d H:i:s')
            ],
            [
                'name'         => Constants::NAME_ROLE_ADMINISTRADOR,
                'display_name' => 'Administrador',
                'description' => 'Has full access to the entire system',
                'created_at'   => date('Y-m-d H:i:s'),
                'updated_at'   => date('Y-m-d H:i:s')
            ],
            [
                'name'         => Constants::NAME_ROLE_PUBLISHER,
                'display_name' => 'Publicador',
                'description' => 'Has limited access to the system',
                'created_at'   =>  date('Y-m-d H:i:s'),
                'updated_at'   =>  date('Y-m-d H:i:s')
            ],
            [
                'name'         => Constants::NAME_ROLE_CLIENT,
                'display_name' => 'Cliente',
                'description' => 'Has limited access to the system',
                'created_at'   => date('Y-m-d H:i:s'),
                'updated_at'   => date('Y-m-d H:i:s')
            ]
        ];

        foreach ($roles as $role){
            DB::table('roles')->insert($role);
        }
    }
}

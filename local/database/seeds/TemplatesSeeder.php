<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plantilla')->insert([
            [
                'slug'        => 'plantilla-1',
                'nombre'      => 'Plantilla 1',
                'path_imagen' => 'plantilla-1.jpg',
                'estado'      => 1,
                'created_at'  => date('Y-m-d H:i:s'),
                'updated_at'  => date('Y-m-d H:i:s')
            ],
            [
                'slug'        => 'plantilla-2',
                'nombre'      => 'Plantilla 2',
                'path_imagen' => 'plantilla-2.jpg',
                'estado'      => 1,
                'created_at'  => date('Y-m-d H:i:s'),
                'updated_at'  => date('Y-m-d H:i:s')
            ],
            [
                'slug'        => 'plantilla-3',
                'nombre'      => 'Plantilla 3',
                'path_imagen' => 'plantilla-3.jpg',
                'estado'      => 1,
                'created_at'  => date('Y-m-d H:i:s'),
                'updated_at'  => date('Y-m-d H:i:s')
            ],
            [
                'slug'        => 'plantilla-4',
                'nombre'      => 'Plantilla 4',
                'path_imagen' => 'plantilla-4.jpg',
                'estado'      => 1,
                'created_at'  => date('Y-m-d H:i:s'),
                'updated_at'  => date('Y-m-d H:i:s')
            ],
        ]);
    }
}

<?php

use App\Globals\Constants;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BulletinStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estados = [
            [
                'slug'   => Constants::NAME_STATUS_BULLETIN_PENDING,
                'nombre' => 'Pendiente',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),            
            ],
            [
                'slug'   => Constants::NAME_STATUS_BULLETIN_APPROVED,
                'nombre' => 'Aprobado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ],
            [
                'slug'   => Constants::NAME_STATUS_BULLETIN_SENT,
                'nombre' => 'Enviado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                
            ],
            [
                'slug'   => Constants::NAME_STATUS_BULLETIN_CANCEL,
                'nombre' => 'Anulado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),            
            ],
        ];

        for($i = 0 ; $i < count($estados) ; $i++) {
            DB::table('boletin_estado')->insert($estados[$i]);
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FacultadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facultades')->insert([
            [
                'slug' => 'facultad-de-artes-asab',
                'nombre' => 'Facultad de Artes ASAB',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'slug' => 'facultad-tecnologia',
                'nombre' => 'Facultad Tecnológica',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'slug' => 'facultad-de-medio-ambiente-y-recursos-naturales',
                'nombre' => 'Facultad de Medio Ambiente y Recursos Naturales',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'slug' => 'facultad-de-ciencias-y-educacion',
                'nombre' => 'Facultad de Ciencias y Educación',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], 
            [
                'slug' => 'facultad-de-ingenieria',
                'nombre' => 'Facultad de Ingeniería',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}

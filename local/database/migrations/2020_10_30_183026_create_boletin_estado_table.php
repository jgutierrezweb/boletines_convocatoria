<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoletinEstadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletin_estado', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug');
            $table->string('nombre');
            $table->tinyInteger('estado')->default('1');
            $table->smallInteger('borrado_logico')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletin_estado');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoletinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletin', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug', 255)->unique();
            $table->string('nombre', 255);
            $table->string('asunto', 255)->nullable();
            $table->string('descripcion', 255)->nullable();
            $table->date('fecha_enviado')->nullable();
            $table->unsignedBigInteger('plantilla_id');
            $table->unsignedBigInteger('boletin_estado_id');
            $table->unsignedInteger('user_id');
            $table->longText('contenido')->nullable();
            $table->string('observaciones', 255)->nullable();
            $table->smallInteger('borrado_logico')->default(0);
            $table->timestamps();

            $table->foreign('plantilla_id', 'boletin_plantilla_id_foreign')
                ->references('id')->on('plantilla')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('boletin_estado_id', 'boletin_boletin_estado_id_foreign')
                ->references('id')->on('boletin_estado')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('user_id', 'boletin_user_id_foreign')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletin');
    }
}

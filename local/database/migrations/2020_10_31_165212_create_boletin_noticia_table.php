<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoletinNoticiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletin_noticia', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->string('slug', 255);
            $table->string('nombre', 255);
            $table->text('descripcion')->nullable();
            $table->string('enlace', 255);
            $table->string('path_imagen', 255);
            $table->unsignedBigInteger('boletin_id');
            $table->tinyInteger('principal')->default(0);
            $table->tinyInteger('estado')->default(1);
            $table->smallInteger('borrado_logico')->default(0);
            $table->timestamps();

            $table->foreign('boletin_id', 'boletin_noticia_boletin_id_foreign')
                ->references('id')->on('boletin')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletin_noticia');
    }
}

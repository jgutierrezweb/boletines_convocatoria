<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->longText('password');            
            $table->string('google_id')->nullable();
            $table->string('avatar')->nullable();
            $table->smallInteger('status')->default(1);
            $table->smallInteger('borrado_logico')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });

        /*Schema::connection('boletines_contratacion_logs')->dropIfExists('users_logs');

        Schema::connection('boletines_contratacion_logs')->create('users_logs', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('role');
            $table->smallInteger('status')->default(1);
            $table->smallInteger('borrado_logico')->default(0);
            $table->text('accion');
            $table->text('usuario');
            $table->integer('user_id');
            $table->rememberToken();
            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

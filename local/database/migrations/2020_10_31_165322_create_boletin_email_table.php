<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoletinEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletin_email', function (Blueprint $table) {
            $table->unsignedBigInteger('boletin_id');
            $table->unsignedBigInteger('email_id');
            $table->tinyInteger('estado')->nullable();

            $table->foreign('boletin_id', 'boletin_email_boletin_id_foreign')
                ->references('id')->on('boletin')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('email_id', 'boletin_email_email_id_foreign')
                ->references('id')->on('email')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletin_email');
    }
}

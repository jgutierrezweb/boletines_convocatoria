<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return redirect()->to('/login');
});

Auth::routes();

Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle')->name('auth-google');
Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback')->name('auth-google-callback');

Route::get('auth/logout', function(Request $requuest){
   \Illuminate\Support\Facades\Auth::logout();
   return redirect('login');
});

Route::middleware(['auth'])->group(function () {
    /*================ HOME ===================*/
    Route::prefix('home')->name('home.')->group(function () {
        Route::get('/', 'HomeController@index')->name('index');
    });

    /*================ BOLETINES ===================*/
    Route::prefix('boletines')->name('newsletters.')->group(function () {
        Route::get('/', 'BoletinesController@index')->middleware(['role:superadministrador|administrador|publicador'])->name('index');
        Route::get('nuevo', 'BoletinesController@create')->middleware(['role:superadministrador|administrador|publicador'])->name('create');
        Route::post('nuevo', 'BoletinesController@store')->middleware(['role:superadministrador|administrador|publicador'])->name('store');
        Route::get('{id}/editar', 'BoletinesController@edit')->middleware(['role:superadministrador|administrador|publicador'])->name('edit');
        Route::post('{id}/editar', 'BoletinesController@update')->middleware(['role:superadministrador|administrador|publicador'])->name('update');
        Route::get('{id}/visualizar', 'BoletinesController@preview')->middleware(['role:superadministrador|administrador|publicador'])->name('preview');
        Route::post('{id}/confirmar', 'BoletinesController@confirm')->middleware(['role:superadministrador|administrador'])->name('confirm');
        Route::post('{id}/anular', 'BoletinesController@cancel')->middleware(['role:superadministrador|administrador'])->name('cancel');

        /*================ NOTICIAS ===================*/
        Route::get('{boletin}/noticias', 'BoletinesController@news')->middleware(['role:superadministrador|administrador|publicador'])->name('news.index');
        Route::post('{boletin}/noticias/{id}/actualizar', 'BoletinesController@updateNew')->middleware(['role:superadministrador|administrador|publicador'])->name('news.update');
        Route::post('{boletin}/noticias/{id}/marcar-principal', 'BoletinesController@markFeaturedNew')->middleware(['role:superadministrador|administrador|publicador'])->name('news.mark-featured');
        Route::post('{boletin}/noticias/{id}/eliminar', 'BoletinesController@deleteNew')->middleware(['role:superadministrador|administrador|publicador'])->name('news.delete');

        /*================ CORREOS ===================*/
        Route::get('{boletin}/correos', 'BoletinesController@emails')->middleware(['role:superadministrador|administrador|publicador'])->name('emails.index');
        Route::post('{boletin}/correos/nuevo', 'BoletinesController@storeEmail')->middleware(['role:superadministrador|administrador|publicador'])->name('emails.store');
        Route::post('{boletin}/correos/{id}/eliminar', 'BoletinesController@deleteEmail')->middleware(['role:superadministrador|administrador|publicador'])->name('emails.delete');
    });

    /*================ PLANTILLAS ===================*/
    Route::prefix('plantillas')->middleware(['role:superadministrador|administrador'])->name('templates.')->group(function () {
        Route::get('/', 'PlantillasController@index')->name('index');
        Route::post('nuevo', 'PlantillasController@nuevo')->name('store');
        Route::post('editarAjax', 'PlantillasController@editarAjax')->name('edit');
        Route::post('editar', 'PlantillasController@editar')->name('update');
    });

    /*================ USUARIOS ===================*/
    Route::prefix('usuarios')->middleware(['role:superadministrador|administrador'])->name('users.')->group(function () {
        Route::get('/', 'UserController@index')->name('index');
        Route::post('nuevo', 'UserController@nuevo')->name('store');
        Route::post('editarAjax', 'UserController@editarAjax')->name('edit');
        Route::post('editar', 'UserController@editar')->name('update');
    });

    /*================ LOGS ===================*/
    Route::prefix('logs')->name('logs.')->group(function () {
        Route::get('/', 'LogsController@index')->name('index');
    });

    /*BORRADO LOGICO*/
    Route::get('borrar/{id?}/{table?}', function($model, $id){
        if(auth()->user()->hasRole(['superadministrador', 'administrador'])) {        
            borradoLogico($model,$id);
            return redirect()->back()->with('message', 'Registro borrado correctamente')->with('messageType', 'success');
        } else {
            abort(403);
        }
    });
});

Route::get('comandos', function () {
    \Artisan::call('config:cache');
    \Artisan::call('config:clear');
    \Artisan::call('route:clear');
    \Artisan::call('view:clear');
    \Artisan::call('cache:clear');

    return "Comandos ejecutados correctamente";
});
